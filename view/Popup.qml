import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2
import dtomas.plugins 1.0

import "."

Window {
    id: popup

    flags: Window.Popup
    color: Style.menuBgColor

    property var item

    x: {
        if (!item) {
            return 0;
        }
        var x;
        if (panel.position == Panel.Right) {
            x = panel.pluginPos.x - width;
        } else if (panel.position == Panel.Left) {
            x = panel.pluginPos.x + panel.height;
        } else {
            x = panel.pluginPos.x + item.x;
        }
        return Math.max(0, Math.min(x, Screen.width - width));
    }
    y: {
        if (!item) {
            return 0;
        }
        var y;
        if (panel.position == Panel.Bottom) {
            y = panel.pluginPos.y - height;
        } else if (panel.position == Panel.Top) {
            y = panel.pluginPos.y + panel.height;
        } else {
            y = panel.pluginPos.y + item.y;
        }
        return Math.max(0, Math.min(y, Screen.height - height));
    }

    width: contentItem.childrenRect.width
    height: contentItem.childrenRect.height

    onVisibleChanged: {
        if (visible) {
            if (Global.currentMenu) {
                Global.currentMenu.hide();
            }
            Global.currentMenu = popup;
        } else {
            Global.currentMenu = null;
        }
    }

    function toggle() {
        if (popup.visible) {
            popup.hide();
        } else {
            if (popup.width > 0 && popup.height > 0) {
                popup.show();
            }
        }
    }
}

