import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

import "."

Popup {
    id: menu

    default property alias children: rows.children

    Column {
        id: rows
        spacing: 0
    }
}
