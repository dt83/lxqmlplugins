import QtQuick 2.3
import QtQuick.Window 2.2
import dtomas.plugins 1.0

import "."

Window {
    id: popup

    flags: Window.Popup
    color: Style.menuBgColor

    property var item
    property var menu

    x: {
        var x;
        if (panel.position == Panel.Right) {
            x = panel.pluginPos.x - width;
        } else if (panel.position == Panel.Left) {
            x = panel.pluginPos.x + panel.height;
        } else {
            x = panel.pluginPos.x + item.x;
        }
        return Math.max(0, Math.min(x, Screen.width - width));
    }
    y: {
        var y;
        if (panel.position == Panel.Bottom) {
            y = panel.pluginPos.y - height;
        } else if (panel.position == Panel.Top) {
            y = panel.pluginPos.y + panel.height;
        } else {
            y = panel.pluginPos.y + item.y;
        }
        return Math.max(0, Math.min(y, Screen.height - height));
    }

    width: contentItem.childrenRect.width
    height: contentItem.childrenRect.height

    Column {

        Repeater {
            model: menu

            delegate: Rectangle {
                id: menuItem

                visible: modelData.visible

                width: row.width + Style.menuMarginRight
                height: row.height

                color: d.mouseOver ? Style.highlightColor : Style.menuBgColor

                Row {
                    id: row
                    spacing: 4
                    Image {
                        id: icon
                        width: modelData.iconSize
                        height: modelData.iconSize
                        opacity: modelData.iconOpacity
                        //source: modelData.iconSource
                    }
                    Text {
                        id: text
                        color: d.mouseOver ? Style.highlightTextColor : Style.textColor
                        anchors.verticalCenter: icon.verticalCenter
                        text: modelData.text
                    }
                }

                //Behavior on color { PropertyAnimation { duration: 50; } }

                QtObject {
                    id: d
                    property bool mouseOver: false
                }

                DraggableMouseArea {
                    id: mouseArea

                    dragMimeData: modelData.dragMimeData

                    anchors.fill: parent
                    hoverEnabled: true
                    preventStealing: true
                    dragItem: icon

                    onClicked: {
                        modelData.clicked();
                    }

                    onEntered: {
                        d.mouseOver = true;
                    }

                    onExited: {
                        d.mouseOver = false;
                    }

                    onDragStarted: {
                        d.mouseOver = false;
                    }

                    onDragFinished: {
                        popup.hide();
                    }
                }
            }
        }
    }
}
