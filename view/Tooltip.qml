import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

import "."

Popup {
    id: tooltip

    property alias text: text.text

    color: Style.menuBgColor

    width: text.width + Style.tooltipMargin * 2
    height: text.height + Style.tooltipMargin * 2

    Text {
        id: text
        anchors.centerIn: parent
        color: Style.textColor
    }
}
