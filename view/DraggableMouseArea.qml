import QtQuick 2.3

import "../utils/utils.js" as Utils

MouseArea {

    property var dragMimeData
    property var dragPixmap
    property var dragItem
    property int dragDistanceThreshold: 16
    property bool movable: false

    signal dragStarted()
    signal dragFinished()
    signal movedLeft()
    signal movedRight()
    signal movedUp()
    signal movedDown()

    property int currentX
    property int currentY

    QtObject {
        id: d

        property var button: null
        property int startX
        property int startY
        property bool pressAndHold: false
        property bool dragging: false
    }

    preventStealing: true
    hoverEnabled: true
    cursorShape: d.pressAndHold ? Qt.ClosedHandCursor : Qt.ArrowCursor

    onPressed: {
        d.button = mouse.button;
        d.startX = mouse.x;
        d.startY = mouse.y;
    }

    onPressAndHold: {
        if (!d.dragging && movable) {
            d.pressAndHold = true;
        }
    }

    onExited: {
        if (d.pressAndHold) {
            var dx = currentX - d.startX;
            var dy = currentY - d.startY;
            if (Math.abs(dx) > Math.abs(dy)) {
                if (dx > 0) {
                    movedRight();
                } else {
                    movedLeft();
                }
            } else {
                if (dy > 0) {
                    movedDown();
                } else {
                    movedUp();
                }
            }
        }
    }

    onPositionChanged: {
        currentX = mouse.x;
        currentY = mouse.y;
        if (!d.pressAndHold && dragMimeData && d.button == Qt.LeftButton &&
                Utils.distance(d.startX, d.startY, mouse.x, mouse.y) >= dragDistanceThreshold) {
            d.dragging = true;
            dragStarted();

            function finishDrag() {
                // HACK to force the mouse area to release the drag.
                visible = false;
                visible = true;
                // END of HACK
                d.button = null;
                d.pressAndHold = false;
                d.dragging = false;
                dragFinished();
            }
            if (dragItem) {
                dragItem.grabToImage(function(result) {
                    dragManager.startDrag(dragMimeData, result);
                    finishDrag();
                });
            } else {
                dragManager.startDrag(dragMimeData, dragPixmap);
                finishDrag();
            }
        }
    }

    onReleased: {
        d.button = null;
        d.pressAndHold = false;
    }

}
