import QtQuick 2.3
import dtomas.plugins 1.0

Item {
    id: arrow

    property color strokeColor
    property color fillColor

    property var position

    Canvas {
        id: canvas

        height: parent.height
        width: parent.width

        antialiasing: true

        onPaint: {
            var ctx = canvas.getContext('2d')
            ctx.reset();

            ctx.strokeStyle = strokeColor;
            ctx.fillStyle = fillColor;
            ctx.lineWidth = 1.25
            ctx.beginPath()
            if (position == Panel.Bottom) {
                ctx.moveTo(0, canvas.height)
                ctx.lineTo(canvas.width / 2, 0)
                ctx.lineTo(canvas.width, canvas.height)
                ctx.lineTo(0, canvas.height)
            } else if (position == Panel.Top) {
                ctx.moveTo(0, 0)
                ctx.lineTo(canvas.width / 2, canvas.height)
                ctx.lineTo(canvas.width, 0)
                ctx.lineTo(0, 0)
            } else if (position == Panel.Right) {
                ctx.moveTo(canvas.width, 0)
                ctx.lineTo(0, canvas.height / 2)
                ctx.lineTo(canvas.width, canvas.height)
                ctx.lineTo(canvas.width, 0)
            } else if (position == Panel.Left) {
                ctx.moveTo(0, 0)
                ctx.lineTo(canvas.width, canvas.height / 2)
                ctx.lineTo(0, canvas.height)
                ctx.lineTo(0, 0)
            }
            ctx.fill();
            ctx.stroke()
        }
    }

    onPositionChanged: {
        canvas.requestPaint();
    }
}
