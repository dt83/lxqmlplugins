import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

import "."

Rectangle {
    id: menuItem

    height: Style.menuSeparatorHeight
    width: parent.width

    color: Style.menuBgColor

    visible: true

    Rectangle {
        anchors.centerIn: parent

        height: 2
        width: parent.width - 8

        color: Style.menuSeparatorColor
    }
}
