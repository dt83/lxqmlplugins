import QtQuick 2.3
import dtomas.plugins 1.0

import "."
//import "../utils/utils.js" as Utils

Item {
    id: icon

    property double zoom: 1.0
    property alias status: image.status
    property alias iconOpacity: image.opacity
    property alias source: image.source
    property alias sourceSize: image.sourceSize
    property alias dragMimeData: mouseArea.dragMimeData
    property alias dragPixmap: mouseArea.dragPixmap
    property string tooltipText
    property var position

    signal leftClicked()
    signal rightClicked()
    signal movedLeft()
    signal movedRight()
    signal wheel(var wheel)

    Behavior on zoom { PropertyAnimation { duration: 50; } }

    QtObject {
        id: d

        property int px: -1
        property int py: -1
        property bool mouseOver: false
        property bool moving: false
    }

    Rectangle {
        anchors.fill: parent

        color: Qt.darker(Style.bgColor1, 2)
        opacity: d.mouseOver ? 0.5 : 0.0

        radius: Style.radius

        //Behavior on opacity { PropertyAnimation { duration: 80; } }
    }

    Image {
        id: image

        anchors.fill: parent

        scale: {
            return zoom;
            if (!d.mouseOver) {
                return zoom;
            }
            var hwidth = image.width / 2;
            var hheight = image.height / 2;
            var fract = 1.0;
            if (position == Panel.Bottom) {
                var dx = mouseArea.currentX - hwidth;
                var dy = mouseArea.currentY - image.height;
                fract = Utils.gauss2d(1.0, hwidth / 2, hheight, dx, dy);
            } else if (position == Panel.Top) {
                var dx = mouseArea.currentX - hwidth;
                var dy = mouseArea.currentY;
                fract = Utils.gauss2d(1.0, hwidth / 2, hheight, dx, dy);
            } else if (position == Panel.Left) {
                var dx = mouseArea.currentX;
                var dy = mouseArea.currentY - hheight;
                fract = Utils.gauss2d(1.0, hwidth, hheight / 2, dx, dy);
            } else if (position == Panel.Right) {
                var dx = mouseArea.currentX - image.width;
                var dy = mouseArea.currentY - hheight;
                fract = Utils.gauss2d(1.0, hwidth, hheight / 2, dx, dy);
            }
            return zoom + (1.0 - zoom) * fract;
        }

        //Behavior on opacity { PropertyAnimation { duration: 50; } }
    }

    DraggableMouseArea {
        id: mouseArea

        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        propagateComposedEvents: true
        movable: true
        dragItem: image

        onEntered: {
            d.mouseOver = true;
            tooltip.text = tooltipText;
            tooltip.item = icon;
            showTooltipTimer.start();
        }

        onExited: {
            d.mouseOver = false;
            showTooltipTimer.stop();
            tooltip.hide();
        }

        onDragFinished: {
            d.mouseOver = false;
            showTooltipTimer.stop();
            tooltip.hide();
        }

        onClicked: {
            if (mouse.button === Qt.LeftButton) {
                leftClicked();
            } else if (mouse.button === Qt.RightButton) {
                rightClicked();
            }
        }

        onMovedLeft: parent.movedLeft()

        onMovedRight: parent.movedRight()

        onWheel: parent.wheel(wheel);
    }

    onTooltipTextChanged: {
        if (tooltip.item == icon) {
            tooltip.text = tooltipText;
        }
    }

    Timer {
        id: showTooltipTimer
        interval: 500
        onTriggered: {
            if (!Global.currentMenu) {
                tooltip.show();
            }
        }
    }
}
