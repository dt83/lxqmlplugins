import QtQuick 2.3
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

import "."

Rectangle {
    id: menuItem

    width: row.width + Style.menuMarginRight
    height: row.height

    color: d.mouseOver ? Style.highlightColor : Style.menuBgColor

    property alias text: text.text
    property int iconSize
    property alias iconOpacity: icon.opacity
    property alias iconSource: icon.source
    property alias dragMimeData: mouseArea.dragMimeData
    property alias dragPixmap: mouseArea.dragPixmap

    signal clicked()
    signal entered()
    signal exited()

    Row {
        id: row
        spacing: 4
        Image {
            id: icon
            width: iconSize
            height: iconSize
            opacity: 1.0
        }
        Text {
            id: text
            color: d.mouseOver ? Style.highlightTextColor : Style.textColor
            anchors.verticalCenter: icon.verticalCenter
        }
    }

    //Behavior on color { PropertyAnimation { duration: 50; } }

    QtObject {
        id: d
        property bool mouseOver: false
    }

    DraggableMouseArea {
        id: mouseArea

        anchors.fill: parent
        hoverEnabled: true
        preventStealing: true
        dragItem: icon

        onClicked: {
            menuItem.clicked();
        }

        onEntered: {
            d.mouseOver = true;
            menuItem.entered();
        }

        onExited: {
            d.mouseOver = false;
            menuItem.exited();
        }

        onDragStarted: {
            d.mouseOver = false;
        }

        onDragFinished: {
            Global.currentMenu.hide();
        }
    }
}
