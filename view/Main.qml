import QtQuick 2.3
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2

import "."

Window {
    id: root

    width: contentItem.childrenRect.width
    height: contentItem.childrenRect.height

    LinearGradient {
        z: -100
        anchors.fill: parent
        start: Qt.point(0, 0)
        end: Qt.point(0, parent.height)
        gradient: Style.bgGradient
    }

}
