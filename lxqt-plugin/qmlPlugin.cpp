#include <QtQml>

#include <lxqt/LXQt/Application>
#include <lxqt/LXQt/Settings>

#include "qmlPlugin.h"

QmlPlugin::QmlPlugin(const ILXQtPanelPluginStartupInfo &startupInfo)
        : ILXQtPanelPlugin(startupInfo) {}

QWidget *QmlPlugin::widget() {
    if (m_widget != nullptr) {
        return m_widget;
    }

    qmlRegisterType<Panel>("dtomas.plugins", 1, 0, "Panel");
    themeChanged();
    m_engine.rootContext()->setContextProperty("panel", &m_panel);
    initQmlEngine(m_engine);

    m_engine.load(qmlUrl());
    m_window = static_cast<QQuickWindow *>(m_engine.rootObjects()[0]);
    m_widget = QWidget::createWindowContainer(m_window);
    m_widget->setMinimumSize(2, 2);
    connect(
        lxqtApp, &LXQt::Application::themeChanged,
        this, &QmlPlugin::themeChanged
    );
    connect(
        &m_panel, &Panel::pluginWidthChanged, this, &QmlPlugin::realign
    );
    realign();
    return m_widget;
}

void QmlPlugin::realign() {
    m_panel.setPosition(static_cast<Panel::Position>(panel()->position()));
    if (m_widget == nullptr) {
        return;
    }
    if (panel()->isHorizontal()) {
        int height = panel()->globalGometry().height();
        m_panel.setHeight(height);
        m_widget->setFixedHeight(height);
        m_widget->setFixedWidth(m_panel.pluginWidth());
        //m_widget->setFixedWidth(m_window->width());
        //m_widget->setFixedWidth(m_window->contentItem()->childrenRect().width());
    } else {
        int width = panel()->globalGometry().width();
        m_panel.setHeight(width);
        m_widget->setFixedWidth(width);
        m_widget->setFixedHeight(m_panel.pluginWidth());
        //m_widget->setFixedHeight(m_window->height());
        //m_widget->setFixedHeight(m_window->contentItem()->childrenRect().height());
    }
    m_panel.setPluginPos(QPoint(m_window->x(), m_window->y()));
}

void QmlPlugin::themeChanged() {
    QQmlComponent styleComponent(
        &m_engine, QUrl("qrc:///style/" + LXQt::LXQtTheme::currentTheme().name() + ".qml")
    );
    if (styleComponent.isError()) {
        styleComponent.loadUrl(QUrl("qrc:///style/BaseStyle.qml"));
    }
    if (m_style != nullptr) {
        m_style->deleteLater();
    }
    m_style = styleComponent.create();
    m_engine.rootContext()->setContextProperty("Style", m_style);
}
