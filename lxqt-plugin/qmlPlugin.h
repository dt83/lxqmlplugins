#ifndef LXQMLPLUGINS_QML_PLUGIN
#define LXQMLPLUGINS_QML_PLUGIN

#include <QObject>
#include <QQuickWindow>
#include <QQmlApplicationEngine>
#include <QWidget>

#include <lxqt/ilxqtpanelplugin.h>

#include "../utils/panel.h"

class QmlPlugin : public QObject, public ILXQtPanelPlugin {
    Q_OBJECT

public:
    QmlPlugin(const ILXQtPanelPluginStartupInfo &startupInfo);

    virtual QWidget *widget();

    virtual ILXQtPanelPlugin::Flags flags() const {
        return 0;
    }

    bool isSeparate() const {
        return true;
    }

    void realign();

    virtual QUrl qmlUrl() const = 0;

protected:
    virtual void initQmlEngine(QQmlApplicationEngine &engine) {
        Q_UNUSED(engine)
    }

private:
    QQuickWindow *m_window = nullptr;
    QWidget *m_widget = nullptr;
    QQmlApplicationEngine m_engine;
    Panel m_panel;
    QObject *m_style = nullptr;

private slots:
    void themeChanged();
};

#endif
