# lxqmlplugins #

Three QML-based plugins for the LXQt panel.

## Pager ##
* Shows miniature desktops.
* Windows can be dragged to other desktops.

## TaskTray ##
* Shows open windows, grouped by application.
* Windows can be dragged to other desktops on Pager.
* Applications can be pinned to create launchers.

## NewsTicker ##
* Shows RSS2 news feeds.

## Intallation ##
1) Install dependencies:
```
#!bash
$ sudo apt-get install liblxqt0-dev libkf5windowsystem-dev libkf5service-dev \
        qml-module-qtquick2 qml-module-qtquick-controls qml-module-qtquick-dialogs
```
2) Build lxqmlplugins:
```
#!bash
$ qmake && make
```
3) Install lxqmlplugins:
```
#!bash
$ sudo make install
```
