import QtQuick 2.3
import dtomas.plugins 1.0

import "view"

Icon {
    id: icon

    width: panel.height
    height: panel.height

    source: "image://winicon/" + (app.appID ? app.appID : app.winID)
    sourceSize.width: panel.height
    sourceSize.height: panel.height

    iconOpacity: app.isOnCurrentDesktop || (app.isPinned && app.winItemCount == 0) ? 1.0 : Style.opacityReduced

    tooltipText: app.title

    dragMimeData: app.mimeData
    position: panel.position

    onMovedLeft: {
        tasktray.apps.moveLeft(index);
    }

    onMovedRight: {
        tasktray.apps.moveRight(index);
    }

    SequentialAnimation on zoom {
        id: attentionAnimation

        running: false
        loops: Animation.Infinite
        PropertyAnimation { duration: 300; to: Style.zoomIconActive}
        PropertyAnimation { duration: 300; to: Style.zoomIconNormal}
    }

    states: [
       State {
           name: "demandsAttention"; when: app.demandsAttention
           PropertyChanges { target: attentionAnimation; running: true; }
       },
       State {
           name: "normal"; when: !app.demandsAttention
           PropertyChanges { target: attentionAnimation; running: false; }
           PropertyChanges {
               target: icon
               zoom: (
                   app.isActive && !app.isMinimized ||
                   appMenu.visible && appMenu.app == app ? Style.zoomIconActive : (
                       app.isMinimized ? Style.zoomIconMinimized : Style.zoomIconNormal
                   )
                )
           }
       }
    ]

    DropArea {
        anchors.fill: parent

        Timer {
            id: timer
            interval: 1500
            repeat: true
            onTriggered: {
                app.activate();
            }
        }

        onEntered: {
            timer.restart();
        }
        onExited: {
            timer.stop();
        }
        onDropped: {
            timer.stop();
        }
    }

    Arrow {
        id: arrow

        SequentialAnimation on opacity {
            id: opacityAnimation

            running: false
            loops: Animation.Infinite
            PropertyAnimation { duration: 300; to: 0.0}
            PropertyAnimation { duration: 300; to: 0.8}
        }

        Behavior on opacity { PropertyAnimation { duration: 300 } }

        states: [
           State {
               name: "starting"; when: app.isStarting
               PropertyChanges { target: opacityAnimation; running: true; }
           },
           State {
               name: "running"; when: !app.isStarting
               PropertyChanges { target: opacityAnimation; running: false; }
               PropertyChanges { target: arrow; opacity: app.winItemCount > 0 ? 0.8 : 0.0 }
           }
        ]

        y: {
           if (panel.position == Panel.Top) {
               return panel.height - panel.height / 6;
           } else if (panel.position == Panel.Bottom) {
               return 0;
           } else {
               return panel.height / 3;
           }
        }
        x: {
           if (panel.position == Panel.Left) {
               return panel.height - panel.height / 6;
           } else if (panel.position == Panel.Right) {
               return 0;
           } else {
               return panel.height / 3;
           }
        }
        width: panel.orientation == Qt.Horizontal ? panel.height / 3 : panel.height / 6
        height: panel.orientation == Qt.Horizontal ? panel.height / 6 : panel.height / 3
        strokeColor: Style.arrowStrokeColor
        fillColor: Style.arrowFillColor
        position: panel.position
    }

    onLeftClicked: {
        var menu = Global.currentMenu;
        if (menu) {
            menu.hide();
        }
        if (menu != tooltip) {
            app.activate();
        }
    }

    onRightClicked: {
        var menu = Global.currentMenu;
        if (menu) {
            menu.hide();
        }
        if (menu != appMenu && appMenu.height > 0) {
            appMenu.item = icon;
            appMenu.app = app;
            appMenu.show();
        }
    }
}
