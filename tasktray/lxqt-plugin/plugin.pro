include(../tasktray.pri)
include(../../lxqt-plugin/plugin.pri)

TARGET = tasktray

HEADERS += tasktrayPlugin.h
SOURCES += tasktrayPlugin.cpp

target.path = $$system(pkg-config --variable=libdir lxqt)/lxqt-panel
target.files = libtasktray.so

desktop.path = "/usr/share/lxqt/lxqt-panel"
desktop.files = tasktray.desktop

INSTALLS += target desktop
