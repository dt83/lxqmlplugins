#include <QtQml>

#include <lxqt/LXQt/Application>

#include "../../utils/appImageProvider.h"
#include "../../utils/themedImageProvider.h"
#include "../../utils/dragManager.h"

#include "tasktrayPlugin.h"

TaskTrayPlugin::TaskTrayPlugin(const ILXQtPanelPluginStartupInfo &startupInfo)
        : QmlPlugin(startupInfo)
        , m_winItemManager()
        , m_appItemManager(&m_winItemManager)
        , m_tasktray(&m_appItemManager) {
    m_translator.load(
        "tasktray_" + QLocale::system().name(), ":/translations"
    );
    lxqtApp->installTranslator(&m_translator);
}

void TaskTrayPlugin::initQmlEngine(QQmlApplicationEngine &engine) {
    qmlRegisterType<TaskTray>();
    qmlRegisterType<DragManager>();
    qmlRegisterType<AppItem>();
    qmlRegisterType<WinItem>();
    qmlRegisterType<WinItemListModel>();
    qmlRegisterType<AppItemListModel>();

    engine.addImageProvider(
        QLatin1String("winicon"),
        new AppImageProvider(&m_winItemManager, &m_appItemManager)
    );
    engine.addImageProvider(QLatin1String("themed"), new ThemedImageProvider());
    engine.rootContext()->setContextProperty("dragManager", &m_dragManager);
    engine.rootContext()->setContextProperty("tasktray", &m_tasktray);
}
