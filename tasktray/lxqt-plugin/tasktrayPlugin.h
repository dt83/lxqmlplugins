#ifndef LXQMLPLUGINS_TASKTRAYPLUGIN_H
#define LXQMLPLUGINS_TASKTRAYPLUGIN_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QTranslator>

#include "../../lxqt-plugin/qmlPlugin.h"
#include "../../utils/dragManager.h"
#include "../tasktray.h"

class TaskTrayPlugin : public QmlPlugin {
    Q_OBJECT

public:
    TaskTrayPlugin(const ILXQtPanelPluginStartupInfo &startupInfo);

    virtual QString themeId() const {
        return "TaskTray";
    }

    virtual QUrl qmlUrl() const {
        return QUrl("qrc:///TaskTray.qml");
    }

    virtual void initQmlEngine(QQmlApplicationEngine &engine);

private:
    WinItemManager m_winItemManager;
    AppItemManager m_appItemManager;
    DragManager m_dragManager;
    TaskTray m_tasktray;
    QTranslator m_translator;

private slots:
    void themeChanged();
};

class TaskTrayPluginLibrary : public QObject, public ILXQtPanelPluginLibrary {
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "lxde-qt.org/Panel/PluginInterface/3.0")
    Q_INTERFACES(ILXQtPanelPluginLibrary)

public:
    ILXQtPanelPlugin *instance(const ILXQtPanelPluginStartupInfo &startupInfo) const {
        return new TaskTrayPlugin(startupInfo);
    }
};


#endif
