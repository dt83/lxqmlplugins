<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../AppMenu.qml" line="38"/>
        <source>Run</source>
        <translation>Ausführen</translation>
    </message>
    <message>
        <location filename="../AppMenu.qml" line="60"/>
        <source>Unpin</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../AppMenu.qml" line="60"/>
        <source>Pin</source>
        <translation>Anheften</translation>
    </message>
</context>
</TS>
