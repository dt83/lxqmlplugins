import QtQuick 2.3
import dtomas.plugins 1.0

import "view"

Menu {
    id: appMenu

    property var app

    Repeater {
        model: app ? app.winItems: []

        delegate: MenuItem {
            text: win.title
            iconSource: "image://winicon/" + app.appID + "/" + win.winID
            iconSize: (
                win.isMinimized ? Style.menuIconSizeMinimized : (
                    win.isActive ? Style.menuIconSizeActive : Style.menuIconSize
                )
            )
            iconOpacity: win.isOnCurrentDesktop ? 1.0 : 0.5

            dragMimeData: win.mimeData

            onClicked: {
                win.activate();
                appMenu.hide();
            }
        }
    }

    MenuSeparator {
        visible: app && app.appID && app.winItemCount > 0
    }

    MenuItem {
        text: qsTr("Run")
        visible: app && app.appID
        iconSize: Style.menuIconSize
        onClicked: { app.run(); appMenu.hide(); }
    }

    Repeater {
        model: app ? app.actions : []

        delegate: MenuItem {
            text: modelData.title
            iconSource: modelData.icon ? "image://themed/" + modelData.icon : ""
            iconSize: Style.menuIconSize

            onClicked: {
                modelData.run();
                appMenu.hide();
            }
        }
    }

    MenuItem {
        text: app && app.isPinned ? qsTr("Unpin") : qsTr("Pin")
        visible: app && !!app.appID
        iconSize: Style.menuIconSize
        onClicked: {
            if (app.isPinned) {
                tasktray.unpinApp(app.appID);
            } else {
                tasktray.pinApp(app.appID);
            }
            appMenu.hide();
        }
    }
}
