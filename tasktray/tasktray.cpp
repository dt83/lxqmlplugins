#include <QJsonObject>
#include <QJsonArray>

#include <kservice.h>

#include "tasktray.h"

TaskTray::TaskTray(AppItemManager *appItemManager, QObject *parent)
        : QObject(parent)
        , m_appItemManager(appItemManager)
        , m_appItems(new AppItemListModel(this))
        , m_config("dtomas.lxqmlplugins", "TaskTray") {
    connect(
        m_appItemManager, &AppItemManager::appItemAdded,
        m_appItems, &AppItemListModel::add
    );

    QJsonDocument doc = m_config.readJSON("tasktray.json");
    if (!doc.isEmpty()) {
        QJsonObject obj = doc.object();
        QJsonArray appsValue = obj.value("apps").toArray();
        for (const QJsonValue &appVal : appsValue) {
            QJsonObject appObj = appVal.toObject();
            QString appID = appObj["appID"].toString();
            m_appItemManager->addAppItem(appID, appObj["className"].toString());
        }
    }
    appItemManager->init();
    connect(
        m_appItems, &QAbstractListModel::rowsMoved,
        this, &TaskTray::save
    );
}

void TaskTray::pinApp(const QString &appID) {
    if (appID.isNull()) {
        return;
    }
    AppItem *appItem = m_appItemManager->getAppItem(appID);
    if (!appItem) {
        return;
    }
    appItem->setIsPinned(true);
    save();
}

void TaskTray::unpinApp(const QString &appID) {
    if (appID.isNull()) {
        return;
    }
    AppItem *appItem = m_appItemManager->getAppItem(appID);
    if (!appItem) {
        return;
    }
    appItem->setIsPinned(false);
    save();
    if (appItem->winItemCount() == 0) {
        m_appItemManager->removeAppItem(appID);
    }
}

QJsonObject TaskTray::toJSON() {
    QJsonObject obj;
    QJsonArray appArray;
    for (const AppItem *appItem : m_appItems->items()) {
        if (!appItem->isPinned()) {
            continue;
        }
        QJsonObject appObj;
        appObj["appID"] = appItem->appID();
        appObj["className"] = appItem->className();
        appArray.append(appObj);
    }
    obj["apps"] = appArray;
    return obj;
}

void TaskTray::save() {
    m_config.writeJSON("tasktray.json", QJsonDocument(toJSON()));
}
