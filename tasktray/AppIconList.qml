import QtQuick 2.3
import QtQuick.Window 2.2
import dtomas.plugins 1.0

import "view"

ListView {
    id: appList

    width: panel.orientation == Qt.Horizontal ? contentItem.childrenRect.width : panel.height
    height: panel.orientation == Qt.Horizontal ? panel.height : contentItem.childrenRect.height

    spacing: 0

    orientation: panel.orientation

    model: tasktray.apps

    AppMenu { id: appMenu }

    Tooltip { id: tooltip }

    delegate: AppIcon { }

    add: Transition {
        NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 200 }
    }

    remove: Transition {
        NumberAnimation { property: "scale"; from: 1.0; to: 0; duration: 200 }
    }

    onWidthChanged: {
        console.log("width changed:", width);
    }
}
