#ifndef LXQMLPLUGINS_TASKTRAY_H
#define LXQMLPLUGINS_TASKTRAY_H

#include <QObject>
#include <QJsonObject>

#include <kwindowsystem.h>
#include <kservice.h>

#include "../model/appItem.h"
#include "../model/winItem.h"
#include "../model/winItemManager.h"
#include "../model/appItemManager.h"
#include "../model/appItemListModel.h"
#include "../utils/config.h"

class TaskTray : public QObject {
    Q_OBJECT

    Q_PROPERTY(AppItemListModel *apps READ apps CONSTANT)

public:
    TaskTray(AppItemManager *appItemManager, QObject *parent = nullptr);

    AppItemListModel *apps() const { return m_appItems; }

    Q_INVOKABLE void pinApp(const QString &appID);
    Q_INVOKABLE void unpinApp(const QString &appID);

    void save();
    QJsonObject toJSON();

private:
    AppItemManager *m_appItemManager;
    AppItemListModel *m_appItems;
    Config m_config;
};

#endif
