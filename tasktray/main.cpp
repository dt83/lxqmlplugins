#include <QtQml>
#include <QUrl>
#include <QQmlApplicationEngine>
#include <QApplication>
#include <QtQuick/QQuickView>
#include <QQuickWindow>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QQmlComponent>

#include <KService>

#include "tasktray.h"
#include "../utils/panel.h"
#include "../utils/dragManager.h"
#include "../utils/appImageProvider.h"
#include "../utils/themedImageProvider.h"

int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
        QLibraryInfo::location(QLibraryInfo::TranslationsPath)
    );
    app.installTranslator(&qtTranslator);

    QTranslator tasktrayTranslator;
    tasktrayTranslator.load(
        "plugins_" + QLocale::system().name(), ":/translations"
    );
    app.installTranslator(&tasktrayTranslator);

    qmlRegisterType<Panel>("dtomas.plugins", 1, 0, "Panel");
    qmlRegisterType<TaskTray>();
    qmlRegisterType<DragManager>();
    qmlRegisterType<AppItem>();
    qmlRegisterType<WinItem>();
    qmlRegisterType<WinItemListModel>();
    qmlRegisterType<AppItemListModel>();

    Panel panel;
    DragManager dragManager;
    WinItemManager winItemManager;
    AppItemManager appItemManager(&winItemManager);
    TaskTray tasktray(&appItemManager);

    QQmlApplicationEngine engine;
    engine.addImageProvider(
        QLatin1String("winicon"),
        new AppImageProvider(&winItemManager, &appItemManager)
    );
    engine.addImageProvider(QLatin1String("themed"), new ThemedImageProvider());
    QQmlComponent styleComponent(&engine, QUrl("qrc:///style/BaseStyle.qml"));
    QObject *style = styleComponent.create();

    engine.rootContext()->setContextProperty("dragManager", &dragManager);
    engine.rootContext()->setContextProperty("tasktray", &tasktray);
    engine.rootContext()->setContextProperty("Style", style);
    engine.rootContext()->setContextProperty("panel", &panel);

    engine.load(QUrl("qrc:///TaskTray.qml"));

    QQuickWindow *window = static_cast<QQuickWindow *>(engine.rootObjects()[0]);
    /*KWindowSystem::setState(
        window->winId(), NET::State::SkipTaskbar | NET::State::SkipPager
    );
    KWindowSystem::setType(window->winId(), NET::WindowType::Dock);
    KWindowSystem::setOnAllDesktops(window->winId(), true);*/
    window->show();
    //KWindowSystem::setStrut(window->winId(), 0, 0, 0, 48);
    window->connect(window, &QWindow::xChanged, &panel, &Panel::setPluginX);
    window->connect(window, &QWindow::yChanged, &panel, &Panel::setPluginY);
    return app.exec();
}
