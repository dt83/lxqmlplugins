include(../lxqmlplugins.pri)

QT += widgets KWindowSystem KService KCoreAddons

HEADERS += \
        $$PWD/tasktray.h \
        $$PWD/../model/baseItem.h \
        $$PWD/../model/appItem.h \
        $$PWD/../model/appAction.h \
        $$PWD/../model/winItem.h \
        $$PWD/../model/simpleListModel.h \
        $$PWD/../model/appItemListModel.h \
        $$PWD/../model/winItemListModel.h \
        $$PWD/../model/winItemManager.h \
        $$PWD/../model/appItemManager.h \
        $$PWD/../utils/dragManager.h \
        $$PWD/../utils/appImageProvider.h \
        $$PWD/../utils/themedImageProvider.h \
        $$PWD/../utils/config.h

SOURCES += \
        $$PWD/tasktray.cpp \
        $$PWD/../model/appItem.cpp \
        $$PWD/../model/appAction.cpp \
        $$PWD/../model/winItem.cpp \
        $$PWD/../model/winItemListModel.cpp \
        $$PWD/../model/winItemManager.cpp \
        $$PWD/../model/appItemManager.cpp \
        $$PWD/../utils/dragManager.cpp \
        $$PWD/../utils/appImageProvider.cpp \
        $$PWD/../utils/themedImageProvider.cpp \
        $$PWD/../utils/config.cpp

lupdate_only {
    SOURCES += $$PWD/*.qml
}

RESOURCES += $$PWD/tasktray.qrc $$PWD/translations/translations.qrc

TRANSLATIONS += $$PWD/translations/tasktray_de.ts
