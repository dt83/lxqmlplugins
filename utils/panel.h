#ifndef LXQMLPLUGINS_PANEL_H
#define LXQMLPLUGINS_PANEL_H

#include <QDebug>
#include <QObject>
#include <QPoint>

class Panel : public QObject {
    Q_OBJECT

    Q_PROPERTY(Qt::Orientation orientation READ orientation NOTIFY positionChanged)
    Q_PROPERTY(Position position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int pluginWidth READ pluginWidth WRITE setPluginWidth NOTIFY pluginWidthChanged)
    Q_PROPERTY(QPoint pluginPos READ pluginPos WRITE setPluginPos NOTIFY pluginPosChanged)

public:
    Panel(QObject *parent = nullptr) : QObject(parent) {}

    enum Position {
        Bottom,
        Top,
        Left,
        Right
    };
    Q_ENUMS(Position)

    Qt::Orientation orientation() const {
        switch (m_position) {
            case Bottom:
            case Top:
                return Qt::Horizontal;
            case Left:
            case Right:
            default:
                return Qt::Vertical;
        }
    }

    Position position() const { return m_position; }
    void setPosition(Position position) {
        m_position = position;
        emit positionChanged();
    }

    int height() const { return m_height; }
    void setHeight(int height) {
        m_height = height;
        emit heightChanged();
    }

    QPoint pluginPos() const { return m_pluginPos; }
    void setPluginPos(const QPoint &pluginPos) {
        m_pluginPos = pluginPos;
        emit pluginPosChanged();
    }
    void setPluginX(int x) {
        m_pluginPos.setX(x);
        emit pluginPosChanged();
    }
    void setPluginY(int y) {
        m_pluginPos.setY(y);
        emit pluginPosChanged();
    }

    int pluginWidth() const { return m_pluginWidth; }
    void setPluginWidth(int width) {
        qDebug() << "plugin width changed:" << width;
        m_pluginWidth = width;
        emit pluginWidthChanged();
    }

signals:
    void positionChanged();
    void heightChanged();
    void pluginPosChanged();
    void pluginWidthChanged();

private:
    Position m_position = Bottom;
    int m_height = 48;
    QPoint m_pluginPos;
    int m_pluginWidth = 2;
};

#endif
