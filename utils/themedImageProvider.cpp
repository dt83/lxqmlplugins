#include <QIcon>

#include "themedImageProvider.h"

QPixmap ThemedImageProvider::requestPixmap(const QString &id, QSize *size,
        const QSize &requestedSize) {
    QPixmap pixmap;
    int width = requestedSize.width() > 0 ? requestedSize.width() : 64;
    int height = requestedSize.height() > 0 ? requestedSize.height() : 64;
    *size = QSize(width, height);

    pixmap = QIcon::fromTheme(id).pixmap(width, height);

    if (pixmap.isNull()) {
        pixmap = QIcon::fromTheme("unknown").pixmap(width, height);
    }

    return pixmap;
}
