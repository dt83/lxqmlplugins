#ifndef LXQMLPLUGINS_DRAGMANAGER_H
#define LXQMLPLUGINS_DRAGMANAGER_H

#include <QObject>
#include <QMimeData>
#include <QPixmap>

class DragManager : public QObject {
    Q_OBJECT

public:
    DragManager(QObject *parent = nullptr) : QObject(parent) {}

    Q_INVOKABLE void startDrag(QMimeData *mimeData, const QPixmap &pixmap);

    Q_INVOKABLE void startDrag(QMimeData *mimeData, QObject *item);

};

#endif
