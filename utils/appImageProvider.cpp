#include <QIcon>

#include "appImageProvider.h"

QPixmap AppImageProvider::requestPixmap(const QString &id, QSize *size,
        const QSize &requestedSize) {
    QPixmap pixmap;
    int width = requestedSize.width() > 0 ? requestedSize.width() : 64;
    int height = requestedSize.height() > 0 ? requestedSize.height() : 64;
    *size = QSize(width, height);

    const QStringList &ids = id.split("/");
    if (ids.size() == 0) {
        return QPixmap();
    }

    QString appID;
    WId winID = 0;

    if (ids.size() == 1) {
        bool isWinID;
        winID = ids[0].toInt(&isWinID);
        if (!isWinID) {
            appID = ids[0];
        }
    } else {
        appID = ids[0];
        winID = ids[1].toInt();
    }

    if (winID != 0 && m_winItemManager != nullptr) {
        pixmap = m_winItemManager->getPixmap(winID, width, height);
    }

    if (pixmap.isNull() && m_appItemManager != nullptr && !appID.isNull()) {
        pixmap = m_appItemManager->getPixmap(appID, width, height);
    }

    if (pixmap.isNull()) {
        pixmap = QIcon::fromTheme("unknown").pixmap(width, height);
    }

    return pixmap;
}
