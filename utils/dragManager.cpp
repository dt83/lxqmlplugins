#include <QDrag>
#include <QQuickItemGrabResult>

#include "dragManager.h"


void DragManager::startDrag(QMimeData *mimeData, const QPixmap &pixmap) {
    if (!mimeData) {
        return;
    }

    // Clone the mime data object, for it is destroyed when the drag has finished.
    QMimeData *mimeDataClone = new QMimeData();
    for (const QString &format : mimeData->formats()) {
        mimeDataClone->setData(format, mimeData->data(format));
    }

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeDataClone);
    drag->setPixmap(pixmap);
    drag->exec(Qt::CopyAction);
}

void DragManager::startDrag(QMimeData *mimeData, QObject *obj) {
    QQuickItemGrabResult *grabResult = qobject_cast<QQuickItemGrabResult *>(obj);
    QPixmap pixmap = QPixmap::fromImage(grabResult->image());
    startDrag(mimeData, pixmap);
}
