#ifndef LXQMLPLUGINS_APP_IMAGE_PROVIDER_H
#define LXQMLPLUGINS_APP_IMAGE_PROVIDER_H

#include <QQuickImageProvider>

#include "../model/appItemManager.h"
#include "../model/winItemManager.h"

class AppImageProvider : public QQuickImageProvider {

public:
    AppImageProvider(WinItemManager *winItemManager,
            AppItemManager *appItemManager = nullptr)
        : QQuickImageProvider(QQuickImageProvider::Pixmap),
          m_winItemManager(winItemManager), m_appItemManager(appItemManager) {}

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);

private:
    WinItemManager *m_winItemManager;
    AppItemManager *m_appItemManager;
};

#endif
