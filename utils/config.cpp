#include <QStandardPaths>
#include <QFile>

#include "config.h"

QDir Config::dir() const {
    QDir configDir(
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
    );
    if (!configDir.exists(m_domain)) {
        configDir.mkdir(m_domain);
    }
    configDir.cd(m_domain);
    if (!configDir.exists(m_project)) {
        configDir.mkdir(m_project);
    }
    configDir.cd(m_project);
    return configDir;
}

QJsonDocument Config::readJSON(const QString &fileName) const {
    QJsonDocument doc;
    QFile configFile(dir().absoluteFilePath(fileName));
    if (configFile.open(QIODevice::ReadOnly)) {
        QByteArray data = configFile.readAll();
        doc = QJsonDocument::fromJson(data);
    }
    return doc;
}

void Config::writeJSON(const QString &fileName, const QJsonDocument doc) const {
    QFile configFile(dir().absoluteFilePath(fileName));
    if (configFile.open(QIODevice::WriteOnly)) {
        configFile.write(doc.toJson());
    }
}
