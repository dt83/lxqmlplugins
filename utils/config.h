#ifndef LXQMLPLUGINS_CONFIG_H
#define LXQMLPLUGINS_CONFIG_H

#include <QDir>
#include <QJsonDocument>

class Config {

public:
    Config(const QString &domain, const QString &project)
        : m_domain(domain), m_project(project) {}

    QDir dir() const;

    QJsonDocument readJSON(const QString &fileName) const;

    void writeJSON(const QString &fileName, const QJsonDocument doc) const;

private:
    QString m_domain, m_project;
};

#endif
