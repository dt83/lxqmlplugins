#ifndef LXQMLPLUGINS_THEMED_IMAGE_PROVIDER_H
#define LXQMLPLUGINS_THEMED_IMAGE_PROVIDER_H

#include <QQuickImageProvider>

class ThemedImageProvider : public QQuickImageProvider {

public:
    ThemedImageProvider() : QQuickImageProvider(QQuickImageProvider::Pixmap) {}

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif
