#include <QIcon>

#include "winImageProvider.h"

QPixmap WinImageProvider::requestPixmap(const QString &id, QSize *size,
        const QSize &requestedSize) {
    QPixmap pixmap;
    int width = requestedSize.width() > 0 ? requestedSize.width() : 64;
    int height = requestedSize.height() > 0 ? requestedSize.height() : 64;
    *size = QSize(width, height);

    bool isWinID;
    WId winID = id.toInt(&isWinID);
    if (!isWinID) {
        return pixmap;
    }

    pixmap = m_winItemManager->getPixmap(winID, width, height);

    if (pixmap.isNull()) {
        pixmap = QIcon::fromTheme("unknown").pixmap(width, height);
    }

    return pixmap;
}
