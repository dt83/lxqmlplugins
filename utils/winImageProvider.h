#ifndef LXQMLPLUGINS_WIN_IMAGE_PROVIDER_H
#define LXQMLPLUGINS_WIN_IMAGE_PROVIDER_H

#include <QQuickImageProvider>

#include "../model/winItemManager.h"

class WinImageProvider : public QQuickImageProvider {

public:
    WinImageProvider(WinItemManager *winItemManager)
        : QQuickImageProvider(QQuickImageProvider::Pixmap),
          m_winItemManager(winItemManager) {}

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);

private:
    WinItemManager *m_winItemManager;
};

#endif
