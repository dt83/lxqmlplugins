

function gauss2d(amplitude, sigmaX, sigmaY, dx, dy) {
  //console.log("gauss2d(" + dx + ", " + dy + ")");
  var exp = -(
    Math.pow(dx, 2) / (2 * Math.pow(sigmaX, 2)) +
    Math.pow(dy, 2) / (2 * Math.pow(sigmaY, 2))
  );
  return amplitude * Math.pow(Math.E, exp);
}

function distance(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
}
