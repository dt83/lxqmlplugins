import QtQuick 2.0

BaseStyle {
    menuSeparatorColor: "#444444"

    bgColor1: "#515048"
    bgColor2: "#3C3B37"
    textColor: "white"
    menuBgColor: "#3c3b37"
    radius: 4
}
