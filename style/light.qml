import QtQuick 2.0
import QtGraphicalEffects 1.0

BaseStyle {
    menuSeparatorColor: "#444444"

    bgGradient: Gradient {
        GradientStop { position: 0.0; color: "#EFEFEF" }
        GradientStop { position: 0.25; color: "#dfdfdf" }
        GradientStop { position: 0.75; color: "#d7d7d7" }
        GradientStop { position: 1.0; color: "#c0c0c0" }
    }
    textColor: palette.text
    highlightColor: palette.highlight
    highlightTextColor: palette.highlightedText
    menuBgColor: palette.base
}
