import QtQuick 2.0

BaseStyle {
    menuSeparatorColor: "#444444"

    bgColor1: "#f0f0f0"
    textColor: "#313639"
    menuBgColor: bgColor1
    highlightTextColor: textColor
    radius: 4
}
