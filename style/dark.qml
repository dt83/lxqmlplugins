import QtQuick 2.0

BaseStyle {
    menuSeparatorColor: "#444444"

    bgColor1: "black"
    textColor: "white"
    highlightColor: "white"
    highlightTextColor: "black"
}
