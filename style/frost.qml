import QtQuick 2.0

BaseStyle {
    menuSeparatorColor: "#444444"

    bgColor1: "#2d2d2d"
    textColor: "white"
    highlightColor: "#1a80b6"
    menuBgColor: bgColor1
    highlightTextColor: textColor
}
