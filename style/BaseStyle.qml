import QtQuick 2.0
import QtGraphicalEffects 1.0

QtObject {
    property var palette: SystemPalette { }

    property int iconSize: 48

    property int menuIconSizeMinimized: 16
    property int menuIconSize: 24
    property int menuIconSizeActive: 32
    property int menuMarginRight: 8
    property int menuSeparatorHeight: 8
    property color menuSeparatorColor: "#444444"

    property int tooltipMargin: 4

    property color bgColor1: palette.base
    property color bgColor2: bgColor1
    property var bgGradient: Gradient {
        GradientStop { position: 0.0; color: bgColor1 }
        GradientStop { position: 1.0; color: bgColor2 }
    }
    property color arrowStrokeColor: "black"
    property color arrowFillColor: "white"
    property color textColor: palette.text
    property color highlightColor: palette.highlight
    property color highlightTextColor: palette.highlightedText
    property color menuBgColor: bgColor1

    property color windowBorderColor: "black"
    property color windowColor: bgColor1
    property color desktopColor: Qt.darker(bgColor1, 2)

    property double opacityReduced: 0.5
    property double zoomIconActive: 1.0
    property double zoomIconNormal: 0.75
    property double zoomIconMinimized: 0.5

    property int radius: 2
}
