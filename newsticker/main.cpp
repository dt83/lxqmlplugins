#include <QtQml>
#include <QUrl>
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QQuickWindow>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

#include "newsTicker.h"
#include "../utils/panel.h"
#include "../utils/themedImageProvider.h"

int main(int argc, char *argv[]) {

    QGuiApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
        QLibraryInfo::location(QLibraryInfo::TranslationsPath)
    );
    app.installTranslator(&qtTranslator);

    QTranslator pagerTranslator;
    pagerTranslator.load(
        "plugins_" + QLocale::system().name(), ":/translations"
    );
    app.installTranslator(&pagerTranslator);

    qmlRegisterType<Panel>("dtomas.plugins", 1, 0, "Panel");
    qmlRegisterType<NewsTicker>();
    qmlRegisterType<NewsItem>();
    qmlRegisterType<NewsFeed>();

    Panel panel;
    NewsTicker newsTicker;

    QQmlApplicationEngine engine;

    engine.addImageProvider(QLatin1String("themed"), new ThemedImageProvider());

    QQmlComponent styleComponent(&engine, QUrl("qrc:///style/ambiance.qml"));
    QObject *style = styleComponent.create();
    engine.rootContext()->setContextProperty("Style", style);
    engine.rootContext()->setContextProperty("panel", &panel);
    engine.rootContext()->setContextProperty("newsTicker", &newsTicker);

    engine.load(QUrl("qrc:///NewsTicker.qml"));
    QQuickWindow *window = static_cast<QQuickWindow *>(engine.rootObjects()[0]);
    /*KWindowSystem::setState(
        window->winId(), NET::State::SkipTaskbar | NET::State::SkipPager
    );
    KWindowSystem::setType(window->winId(), NET::WindowType::Dock);
    KWindowSystem::setOnAllDesktops(window->winId(), true);*/
    window->show();
    panel.setPluginPos(QPoint(window->x(), window->y()));
    window->connect(window, &QWindow::xChanged, &panel, &Panel::setPluginX);
    window->connect(window, &QWindow::yChanged, &panel, &Panel::setPluginY);
    return app.exec();
}
