import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "view"

Popup {
    id: popup

    property var newsItem

    width: 600
    height: 200

    TextArea {
        id: description
        anchors.fill: parent

        readOnly: true

        text: newsItem ? (
            newsItem.description + "<br/><br/>" +
            Qt.formatDateTime(newsItem.pubDate, Qt.DefaultLocaleLongDate)
        ) : ""
        textFormat: TextEdit.RichText

        style: TextAreaStyle {
            backgroundColor: Style.menuBgColor
            textColor: Style.textColor
        }

        onLinkActivated: {
            newsTicker.activateLink(link);
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true

            onEntered: {
                hidePopupTimer.stop();
            }
            onExited: {
                hidePopupTimer.start();
            }
            onClicked: mouse.accepted = false;
            onPressed: mouse.accepted = false;
            onReleased: mouse.accepted = false;
            onDoubleClicked: mouse.accepted = false;
            onPositionChanged: mouse.accepted = false;
            onPressAndHold: mouse.accepted = false;
        }
    }

    function showDelayed() {
        hidePopupTimer.stop();
        showPopupTimer.start();
    }

    function hideDelayed() {
        showPopupTimer.stop();
        hidePopupTimer.start();
    }

    Timer {
        id: hidePopupTimer
        interval: 500
        onTriggered: popup.hide();
    }

    Timer {
        id: showPopupTimer
        interval: 500
        onTriggered: {
            hidePopupTimer.stop();
            popup.show()
        }
    }
}
