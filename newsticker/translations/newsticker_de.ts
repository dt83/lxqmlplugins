<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>NewsTicker</name>
    <message>
        <location filename="../NewsTicker.qml" line="47"/>
        <source>Remove feed</source>
        <translation>Feed entfernen</translation>
    </message>
    <message>
        <location filename="../NewsTicker.qml" line="58"/>
        <location filename="../NewsTicker.qml" line="86"/>
        <source>Add feed</source>
        <translation>Feed hinzufügen</translation>
    </message>
    <message>
        <location filename="../NewsTicker.qml" line="73"/>
        <source>Confirm removal</source>
        <translation>Entfernen bestätigen</translation>
    </message>
    <message>
        <location filename="../NewsTicker.qml" line="74"/>
        <source>Really remove news feed %1 (%2)?</source>
        <translation>Feed %1 (%2) wirklich entfernen?</translation>
    </message>
    <message>
        <location filename="../NewsTicker.qml" line="93"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../NewsTicker.qml" line="157"/>
        <source>Fetching news...</source>
        <translation>Hole Nachrichten...</translation>
    </message>
</context>
</TS>
