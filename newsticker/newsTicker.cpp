#include <QTime>
#include <QStandardPaths>
#include <QJsonArray>
#include <QDesktopServices>

#include "newsTicker.h"

NewsTicker::NewsTicker(QObject *parent)
        : QObject(parent), m_config("dtomas.lxqmlplugins", "NewsTicker") {
    qsrand(QTime::currentTime().msec());
    QJsonDocument doc = m_config.readJSON("feeds.json");
    if (!doc.isEmpty()) {
        QJsonArray feeds = doc.array();
        for (const QJsonValue &feedVal : feeds) {
            m_newsItemManager.addFeed(feedVal.toString());
        }
    }
    connect(
        &m_newsItemManager, &NewsItemManager::ready,
        this, &NewsTicker::newsReady
    );
}

NewsTicker::~NewsTicker() {
    setCurrentItem(nullptr);
}

void NewsTicker::newsReady() {
    if (m_currentItem == nullptr) {
        m_itemIndex = -1;
        nextItem();
    }
}

void NewsTicker::update() {
    m_newsItemManager.update();
}

void NewsTicker::currentItemDestroyed() {
    setCurrentItem(nullptr);
}

void NewsTicker::setCurrentItem(NewsItem *item) {
    if (m_currentItem != nullptr) {
        disconnect(
            m_currentItem, &QObject::destroyed,
            this, &NewsTicker::currentItemDestroyed
        );
    }
    m_currentItem = item;
    if (m_currentItem != nullptr) {
        item->setSeen(true);
        connect(
            m_currentItem, &QObject::destroyed,
            this, &NewsTicker::currentItemDestroyed
        );
        emit currentItemChanged();
    }
}

void NewsTicker::previousItem() {
    if (m_itemIndex == 0) {
        m_itemIndex = m_newsItemManager.itemCount() - 1;
    } else {
        m_itemIndex--;
    }
    setCurrentItem(m_newsItemManager.getItem(m_itemIndex));
}

void NewsTicker::nextItem(bool preferUnseen) {
    NewsItem *item = nullptr;
    NewsItem *firstItem = nullptr;
    do {
        if (m_itemIndex == m_newsItemManager.itemCount() - 1) {
            m_itemIndex = 0;
        } else {
            m_itemIndex++;
        }
        if (firstItem == nullptr) {
            firstItem = item;
        }
        item = m_newsItemManager.getItem(m_itemIndex);
    } while (preferUnseen && item != firstItem && !item->seen());
    setCurrentItem(item);
}

void NewsTicker::nextFeed() {
    NewsItem *item = m_currentItem;
    NewsItem *firstItem = m_currentItem;
    NewsFeed *feed = m_newsItemManager.getNextFeed(m_currentItem->feed());
    do {
        if (m_itemIndex == m_newsItemManager.itemCount() - 1) {
            m_itemIndex = 0;
        } else {
            m_itemIndex++;
        }
        if (firstItem == nullptr) {
            firstItem = item;
        }
        item = m_newsItemManager.getItem(m_itemIndex);
    } while (item->feed() != feed && item != firstItem);
    setCurrentItem(item);
}

void NewsTicker::previousFeed() {
    NewsItem *item = m_currentItem;
    NewsItem *firstItem = m_currentItem;
    NewsFeed *feed = m_newsItemManager.getPreviousFeed(m_currentItem->feed());
    do {
        if (m_itemIndex == 0) {
            m_itemIndex = m_newsItemManager.itemCount() - 1;
        } else {
            m_itemIndex--;
        }
        item = m_newsItemManager.getItem(m_itemIndex);
    } while (item->feed() != feed && item != firstItem);
    setCurrentItem(m_newsItemManager.getItem(m_itemIndex));
}

void NewsTicker::randomItem() {
    m_itemIndex = qrand() % m_newsItemManager.itemCount();
    setCurrentItem(m_newsItemManager.getItem(m_itemIndex));
}

void NewsTicker::activateLink(const QString &link) {
    QDesktopServices::openUrl(link);
}

void NewsTicker::removeFeed(const QString &url) {
    m_newsItemManager.removeFeed(url);
    save();
    nextItem();
}

void NewsTicker::addFeed(const QString &url) {
    m_newsItemManager.addFeed(url);
    save();
}

QJsonArray NewsTicker::toJSON() const {
    QJsonArray feedArray;
    for (const QUrl &feedUrl : m_newsItemManager.feedUrls()) {
        feedArray.append(feedUrl.toString());
    }
    return feedArray;
}

void NewsTicker::save() const {
    m_config.writeJSON("feeds.json", QJsonDocument(toJSON()));
}
