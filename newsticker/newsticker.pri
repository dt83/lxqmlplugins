include(../lxqmlplugins.pri)

QT += xml

HEADERS += \
        $$PWD/newsTicker.h \
        $$PWD/../model/newsItem.h \
        $$PWD/../model/newsFeed.h \
        $$PWD/../model/newsItemManager.h \
        $$PWD/../utils/themedImageProvider.h \
        $$PWD/../utils/config.h

SOURCES += \
        $$PWD/newsTicker.cpp \
        $$PWD/../model/newsItem.cpp \
        $$PWD/../model/newsFeed.cpp \
        $$PWD/../model/newsItemManager.cpp \
        $$PWD/../utils/themedImageProvider.cpp \
        $$PWD/../utils/config.cpp

RESOURCES += $$PWD/newsTicker.qrc $$PWD/translations/translations.qrc

lupdate_only {
    SOURCES += $$PWD/*.qml
}

TRANSLATIONS += $$PWD/translations/newsticker_de.ts
