#ifndef LXQMLPLUGINS_NEWSTICKERPLUGIN_H
#define LXQMLPLUGINS_NEWSTICKERPLUGIN_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QTranslator>

#include "../newsTicker.h"
#include "../../lxqt-plugin/qmlPlugin.h"

class NewsTickerPlugin : public QmlPlugin {
    Q_OBJECT

public:
    NewsTickerPlugin(const ILXQtPanelPluginStartupInfo &startupInfo);

    virtual QString themeId() const {
        return "NewsTicker";
    }

    virtual QUrl qmlUrl() const {
        return QUrl("qrc:///NewsTicker.qml");
    }

    virtual void initQmlEngine(QQmlApplicationEngine &engine);

    virtual ILXQtPanelPlugin::Flags flags() const {
        return ILXQtPanelPlugin::Flag::PreferRightAlignment;
    }

private:
    NewsTicker m_newsTicker;
    QTranslator m_translator;
};

class NewsTickerPluginLibrary : public QObject, public ILXQtPanelPluginLibrary {
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "lxde-qt.org/Panel/PluginInterface/3.0")
    Q_INTERFACES(ILXQtPanelPluginLibrary)

public:
    ILXQtPanelPlugin *instance(const ILXQtPanelPluginStartupInfo &startupInfo) const {
        return new NewsTickerPlugin(startupInfo);
    }
};


#endif
