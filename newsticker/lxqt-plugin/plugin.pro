include(../newsticker.pri)
include(../../lxqt-plugin/plugin.pri)

TARGET = newsticker

HEADERS += newsTickerPlugin.h
SOURCES += newsTickerPlugin.cpp

target.path = $$system(pkg-config --variable=libdir lxqt)/lxqt-panel
target.files = libnewsticker.so
INSTALLS += target

desktop.path = "/usr/share/lxqt/lxqt-panel"
desktop.files = newsticker.desktop
INSTALLS += desktop
