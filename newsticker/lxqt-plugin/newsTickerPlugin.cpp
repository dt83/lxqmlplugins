#include <QtQml>

#include <lxqt/LXQt/Application>

#include "newsTickerPlugin.h"
#include "../../utils/themedImageProvider.h"

NewsTickerPlugin::NewsTickerPlugin(const ILXQtPanelPluginStartupInfo &startupInfo)
        : QmlPlugin(startupInfo) {
    m_translator.load(
        "newsticker_" + QLocale::system().name(), ":/translations"
    );
    lxqtApp->installTranslator(&m_translator);
}

void NewsTickerPlugin::initQmlEngine(QQmlApplicationEngine &engine) {
    qmlRegisterType<NewsTicker>();

    engine.addImageProvider(QLatin1String("themed"), new ThemedImageProvider());
    engine.rootContext()->setContextProperty("newsTicker", &m_newsTicker);
}
