#ifndef LXQMLPLUGINS_NEWS_TICKER_H
#define LXQMLPLUGINS_NEWS_TICKER_H

#include <QObject>
#include <QJsonArray>

#include "../model/newsItem.h"
#include "../model/newsItemManager.h"
#include "../utils/config.h"

class NewsTicker : public QObject {
    Q_OBJECT

    Q_PROPERTY(NewsItem *currentItem READ currentItem NOTIFY currentItemChanged)

public:
    NewsTicker(QObject *parent = nullptr);
    ~NewsTicker();

    NewsItem *currentItem() const { return m_currentItem; }

    Q_INVOKABLE void randomItem();

    Q_INVOKABLE void nextItem(bool preferUnseen = false);

    Q_INVOKABLE void previousItem();

    Q_INVOKABLE void update();

    Q_INVOKABLE void activateLink(const QString &link);

    Q_INVOKABLE void removeFeed(const QString &url);

    Q_INVOKABLE void addFeed(const QString &url);

    Q_INVOKABLE void nextFeed();

    Q_INVOKABLE void previousFeed();

    QJsonArray toJSON() const;

    void save() const;

signals:
    void currentItemChanged();

private:
    NewsItem *m_currentItem = nullptr;
    NewsItemManager m_newsItemManager;
    int m_itemIndex = 0;
    Config m_config;

    void setCurrentItem(NewsItem *item);

private slots:
    void newsReady();
    void currentItemDestroyed();
};

#endif
