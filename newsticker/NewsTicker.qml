import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

import dtomas.plugins 1.0

import "view"

Main {
    id: root

    property var item: newsTicker.currentItem

    Tooltip { id: tooltip }

    Item {
        objectName: "mainItem"

        width: row.width
        height: row.height


        Menu {
            id: feedMenu
            item: icon

            Repeater {
                model: item ? item.feed.items.slice(0, 8) : null

                MenuItem {
                    id: newsMenuItem
                    text: model.modelData.headline
                    iconSize: 24

                    onClicked: {
                        model.modelData.open();
                        feedMenu.hide();
                    }
                }
            }

            MenuSeparator {}

            MenuItem {
                text: qsTr("Remove feed")
                iconSize: 24

                onClicked: {
                    removeConfirmDialog.feed = item.feed;
                    removeConfirmDialog.open();
                    feedMenu.hide();
                }
            }

            MenuItem {
                text: qsTr("Add feed")
                iconSize: 24

                onClicked: {
                    addFeedDialog.open();
                    feedMenu.hide();
                }
            }
        }

        MessageDialog {
            id: removeConfirmDialog

            property var feed

            title: qsTr("Confirm removal")
            text: qsTr("Really remove news feed %1 (%2)?").arg(feed.title).arg(feed.url)

            standardButtons: StandardButton.Cancel | StandardButton.Ok

            onAccepted: {
                newsTicker.removeFeed(feed.url);
            }
        }

        Dialog {
            id: addFeedDialog

            title: qsTr("Add feed")

            width: 500

            Row {
                spacing: 4

                Text { text: qsTr("URL") + ":" }

                TextField {
                    id: urlInput
                    width: 400
                }
            }

            standardButtons: StandardButton.Cancel | StandardButton.Ok

            onAccepted: {
                newsTicker.addFeed(urlInput.text);
            }
        }

        Row {
            id: row

            height: panel.height

            spacing: 4

            anchors.verticalCenter: parent.verticalCenter

            Icon {
                id: icon

                anchors.verticalCenter: parent.verticalCenter

                source: icon.status != Image.Error && item ? item.feed.iconUrl : "image://themed/unknown"
                width: 24
                height: 24
                sourceSize.width: 24
                sourceSize.height: 24
                tooltipText: item ? item.feed.title : ""

                onLeftClicked: {
                    item.feed.open();
                }

                onRightClicked: {
                    var menu = Global.currentMenu;
                    if (menu) {
                        menu.hide();
                    }
                    if (menu != feedMenu) {
                        feedMenu.show();
                    }
                }

                onWheel: {
                    if (wheel.angleDelta.y > 0) {
                        newsTicker.nextFeed();
                    } else {
                        newsTicker.previousFeed();
                    }
                }
            }

            Text {
                id: headline

                anchors.verticalCenter: parent.verticalCenter

                text: item ? item.headline : qsTr("Fetching news...")
                color: Style.textColor

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true

                    onClicked: {
                        item.open();
                    }

                    onEntered: {
                        popup.showDelayed();
                        nextItemTimer.stop();
                    }
                    onExited: {
                        popup.hideDelayed();
                    }
                    onWheel: {
                        if (wheel.angleDelta.y > 0) {
                            newsTicker.nextItem();
                        } else {
                            newsTicker.previousItem();
                        }
                    }
                }
            }
        }

        Timer {
            id: nextItemTimer

            // Switch to next item after 20 seconds.
            interval: 20000
            repeat: true
            running: true
            onTriggered: {
                newsTicker.nextItem(true);
            }
        }

        Timer {
            id: updateTimer

            // Update every 10 minutes.
            interval: 600000
            repeat: true
            running: true
            onTriggered: {
                newsTicker.update();
            }
        }

        NewsPopup {
            id: popup
            item: row
            newsItem: newsTicker.currentItem

            onVisibleChanged: {
                if (visible) {
                    nextItemTimer.stop();
                } else {
                    nextItemTimer.start();
                }
            }
        }

        onWidthChanged: {
            if (panel.orientation == Qt.Horizontal) {
                panel.pluginWidth = width;
            }
        }

        onHeightChanged: {
            if (panel.orientation == Qt.Vertical) {
                panel.pluginWidth = height;
            }
        }
    }
}
