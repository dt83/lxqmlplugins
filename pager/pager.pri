include(../lxqmlplugins.pri)

QT += KWindowSystem

HEADERS += \
        $$PWD/pager.h \
        $$PWD/../model/baseItem.h \
        $$PWD/../model/winItem.h \
        $$PWD/../model/desktopItem.h \
        $$PWD/../model/simpleListModel.h \
        $$PWD/../model/desktopItemListModel.h \
        $$PWD/../model/winItemListModel.h \
        $$PWD/../model/winItemManager.h \
        $$PWD/../model/desktopItemManager.h \
        $$PWD/../utils/winImageProvider.h \
        $$PWD/../utils/dragManager.h

SOURCES += \
        $$PWD/pager.cpp \
        $$PWD/../model/winItem.cpp \
        $$PWD/../model/desktopItem.cpp \
        $$PWD/../model/winItemListModel.cpp \
        $$PWD/../model/winItemManager.cpp \
        $$PWD/../model/desktopItemManager.cpp \
        $$PWD/../utils/winImageProvider.cpp \
        $$PWD/../utils/dragManager.cpp

RESOURCES += $$PWD/pager.qrc
