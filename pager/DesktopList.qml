import QtQuick 2.3
import QtQuick.Window 2.2

import "view"

Item {
    id: desktopList

    width: panel.orientation == Qt.Horizontal ? grid.width : panel.height - 12
    height: panel.orientation == Qt.Horizontal ? panel.height : grid.height

    property double aspectRatio: Screen.width / Screen.height
    property int desktopWidth: panel.orientation == Qt.Vertical ? panel.height : panel.height * aspectRatio
    property int desktopHeight: panel.orientation == Qt.Horizontal ? panel.height : panel.height * aspectRatio

    Grid {
        id: grid

        width: panel.orientation == Qt.Vertical ? panel.height : childrenRect.width
        height: panel.orientation == Qt.Horizontal ? panel.height : childrenRect.height

        columns: panel.orientation == Qt.Vertical ? 1 : pager.numberOfDesktops
        rows: panel.orientation == Qt.Horizontal ? 1 : pager.numberOfDesktops

        spacing: 2

        Repeater {

            model: pager.desktops

            delegate: Rectangle {

                clip: true
                color: Style.desktopColor

                width: panel.orientation == Qt.Horizontal ? parent.height * aspectRatio : parent.width
                height: panel.orientation == Qt.Horizontal ? parent.height : parent.width / aspectRatio

                property double desktopScale: width / Screen.width

                DraggableMouseArea {
                    anchors.fill: parent
                    propagateComposedEvents: true
                    movable: true

                    onMovedLeft: {
                        pager.moveDesktopLeft(index + 1);
                    }

                    onMovedRight: {
                        pager.moveDesktopRight(index + 1);
                    }
                    onClicked: {
                        desktop.activate();
                    }
                }

                Repeater {
                    model: desktop.windows

                    delegate: Rectangle {
                        id: winRect

                        opacity: win.isMinimized ? 0.15 : 1.0

                        property rect geometry: win.geometry

                        x: geometry.x * desktopScale
                        y: geometry.y * desktopScale
                        width: geometry.width * desktopScale
                        height: geometry.height * desktopScale

                        border.width: 1
                        border.color: Style.windowBorderColor

                        color: Style.windowColor

                        Image {
                            id: image

                            anchors.centerIn: parent
                            source: "image://winicon/" + win.winID
                            sourceSize.width: width
                            sourceSize.height: height
                            width: Math.min(
                                panel.height / 2,
                                Math.min(winRect.width, winRect.height) * 0.75
                            )
                            height: width
                        }

                        DraggableMouseArea {
                            anchors.fill: parent
                            dragMimeData: win.mimeData
                            dragItem: parent
                            propagateComposedEvents: true
                            movable: true

                            onMovedLeft: {
                                pager.moveDesktopLeft(win.desktop);
                            }

                            onMovedRight: {
                                pager.moveDesktopRight(win.desktop);
                            }
                        }
                    }
                }

                Rectangle {
                    anchors.fill: parent
                    color: Style.highlightColor
                    opacity: desktop.isCurrent ? 0.25 : 0.0

                    //Behavior on opacity { PropertyAnimation { duration: 100; } }
                }

                DropArea {
                    anchors.fill: parent

                    Timer {
                        id: timer
                        interval: 1500
                        onTriggered: {
                            desktop.activate();
                        }
                    }

                    onEntered: {
                        timer.restart();
                    }
                    onExited: {
                        timer.stop();
                    }
                    onDropped: {
                        timer.stop();
                        var winID = drop.getDataAsArrayBuffer("application/x-wnck-window-id");
                        pager.dropWnckWindowID(winID, desktop.index);
                    }
                }
            }
        }
    }
}
