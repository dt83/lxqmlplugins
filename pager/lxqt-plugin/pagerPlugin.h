#ifndef LXQMLPLUGINS_PAGERPLUGIN_H
#define LXQMLPLUGINS_PAGERPLUGIN_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

#include "../pager.h"
#include "../../lxqt-plugin/qmlPlugin.h"
#include "../../utils/dragManager.h"

class PagerPlugin : public QmlPlugin {
    Q_OBJECT

public:
    PagerPlugin(const ILXQtPanelPluginStartupInfo &startupInfo);

    virtual QString themeId() const {
        return "Pager";
    }

    virtual QUrl qmlUrl() const {
        return QUrl("qrc:///Pager.qml");
    }

    virtual void initQmlEngine(QQmlApplicationEngine &engine);

private:
    WinItemManager m_winItemManager;
    DesktopItemManager m_desktopItemManager;
    DragManager m_dragManager;
    Pager m_pager;
};

class PagerPluginLibrary : public QObject, public ILXQtPanelPluginLibrary {
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "lxde-qt.org/Panel/PluginInterface/3.0")
    Q_INTERFACES(ILXQtPanelPluginLibrary)

public:
    ILXQtPanelPlugin *instance(const ILXQtPanelPluginStartupInfo &startupInfo) const {
        return new PagerPlugin(startupInfo);
    }
};


#endif
