include(../pager.pri)
include(../../lxqt-plugin/plugin.pri)

TARGET = pager

HEADERS += pagerPlugin.h
SOURCES += pagerPlugin.cpp

target.path = $$system(pkg-config --variable=libdir lxqt)/lxqt-panel
target.files = libpager.so
INSTALLS += target

desktop.path = "/usr/share/lxqt/lxqt-panel"
desktop.files = pager.desktop
INSTALLS += desktop
