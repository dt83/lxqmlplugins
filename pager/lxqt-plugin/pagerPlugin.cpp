#include <QtQml>

#include <lxqt/LXQt/Application>

#include "../../utils/winImageProvider.h"
#include "pagerPlugin.h"

PagerPlugin::PagerPlugin(const ILXQtPanelPluginStartupInfo &startupInfo)
        : QmlPlugin(startupInfo)
        , m_desktopItemManager(&m_winItemManager)
        , m_pager(&m_desktopItemManager) {
}

void PagerPlugin::initQmlEngine(QQmlApplicationEngine &engine) {
    qmlRegisterType<Pager>();
    qmlRegisterType<DragManager>();
    qmlRegisterType<DesktopItem>();
    qmlRegisterType<WinItem>();
    qmlRegisterType<WinItemListModel>();
    qmlRegisterType<DesktopItemListModel>();

    engine.rootContext()->setContextProperty("pager", &m_pager);
    engine.rootContext()->setContextProperty("dragManager", &m_dragManager);
    engine.addImageProvider(
        QLatin1String("winicon"), new WinImageProvider(&m_winItemManager)
    );
}
