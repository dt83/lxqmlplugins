#ifndef LXQMLPLUGINS_PAGER_H
#define LXQMLPLUGINS_PAGER_H

#include <QObject>

#include "../model/desktopItem.h"
#include "../model/winItem.h"
#include "../model/winItemManager.h"
#include "../model/desktopItemManager.h"
#include "../model/desktopItemListModel.h"

class Pager : public QObject {
    Q_OBJECT

    Q_PROPERTY(DesktopItemListModel *desktops READ desktops CONSTANT)
    Q_PROPERTY(int numberOfDesktops READ numberOfDesktops NOTIFY numberOfDesktopsChanged)

public:
    Pager(DesktopItemManager *desktopItemManager, QObject *parent = nullptr);

    DesktopItemListModel *desktops() const { return m_desktops; }

    Q_INVOKABLE void dropWnckWindowID(const QByteArray &winIDArray, int desktop);

    int numberOfDesktops() const;

    Q_INVOKABLE void moveDesktopLeft(int index);

    Q_INVOKABLE void moveDesktopRight(int index);

signals:
    void numberOfDesktopsChanged();

private:
    DesktopItemManager *m_desktopItemManager;
    DesktopItemListModel *m_desktops;

private slots:
    void desktopItemAdded(DesktopItem *item);
};

#endif
