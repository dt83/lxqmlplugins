#include <QtQml>
#include <QUrl>
#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QQuickWindow>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

#include "pager.h"
#include "../model/desktopItemListModel.h"
#include "../model/desktopItemManager.h"
#include "../utils/dragManager.h"
#include "../utils/panel.h"
#include "../utils/winImageProvider.h"

int main(int argc, char *argv[]) {

    QGuiApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
        QLibraryInfo::location(QLibraryInfo::TranslationsPath)
    );
    app.installTranslator(&qtTranslator);

    QTranslator pagerTranslator;
    pagerTranslator.load(
        "plugins_" + QLocale::system().name(), ":/translations"
    );
    app.installTranslator(&pagerTranslator);

    qmlRegisterType<Panel>("dtomas.plugins", 1, 0, "Panel");
    qmlRegisterType<Pager>();
    qmlRegisterType<DragManager>();
    qmlRegisterType<DesktopItem>();
    qmlRegisterType<WinItem>();
    qmlRegisterType<WinItemListModel>();
    qmlRegisterType<DesktopItemListModel>();

    Panel panel;
    WinItemManager winItemManager;
    DesktopItemManager desktopItemManager(&winItemManager);
    DesktopItemListModel desktops;
    Pager pager(&desktopItemManager);

    QQmlApplicationEngine engine;
    engine.addImageProvider(
        QLatin1String("winicon"), new WinImageProvider(&winItemManager)
    );

    QQmlComponent styleComponent(&engine, QUrl("qrc:///style/ambiance.qml"));
    QObject *style = styleComponent.create();
    engine.rootContext()->setContextProperty("Style", style);
    engine.rootContext()->setContextProperty("panel", &panel);

    DragManager dragManager;
    engine.rootContext()->setContextProperty("dragManager", &dragManager);

    engine.rootContext()->setContextProperty("pager", &pager);
    engine.load(QUrl("qrc:///Pager.qml"));
    QQuickWindow *window = static_cast<QQuickWindow *>(engine.rootObjects()[0]);
    /*KWindowSystem::setState(
        window->winId(), NET::State::SkipTaskbar | NET::State::SkipPager
    );
    KWindowSystem::setType(window->winId(), NET::WindowType::Dock);
    KWindowSystem::setOnAllDesktops(window->winId(), true);*/
    window->show();
    return app.exec();
}
