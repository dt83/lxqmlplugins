#include <kwindowsystem.h>

#include "pager.h"

Pager::Pager(DesktopItemManager *desktopItemManager, QObject *parent)
        : QObject(parent)
        , m_desktopItemManager(desktopItemManager)
        , m_desktops(new DesktopItemListModel(this)) {
    m_desktopItemManager->init();
    for (DesktopItem *desktopItem : m_desktopItemManager->desktopItems()) {
        m_desktops->add(desktopItem);
    }
    connect(
        KWindowSystem::self(), &KWindowSystem::numberOfDesktopsChanged,
        this, &Pager::numberOfDesktopsChanged
    );
    connect(
        m_desktopItemManager, &DesktopItemManager::desktopItemAdded,
        this, &Pager::desktopItemAdded
    );
}

void Pager::dropWnckWindowID(const QByteArray &winIDArray, int desktop) {
    WId winID;
    memcpy(&winID, winIDArray.data(), sizeof(WId));
    KWindowSystem::setOnDesktop(winID, desktop);
}

int Pager::numberOfDesktops() const {
    return KWindowSystem::numberOfDesktops();
}

void Pager::desktopItemAdded(DesktopItem *item) {
    m_desktops->add(item);
}


void Pager::moveDesktopLeft(int index) {
    m_desktopItemManager->swapDesktops(index, index - 1);
}

void Pager::moveDesktopRight(int index) {
    m_desktopItemManager->swapDesktops(index, index + 1);
}
