import QtQuick 2.3

import "view"

Main {
    id: root

    DesktopList {
        id: desktopList

        onWidthChanged: {
            if (panel.orientation == Qt.Horizontal) {
                panel.pluginWidth = width;
            }
        }

        onHeightChanged: {
            if (panel.orientation == Qt.Vertical) {
                panel.pluginWidth = height;
            }
        }
    }
}
