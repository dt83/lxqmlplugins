#ifndef LXQMLPLUGINS_APPITEMLISTMODEL_H
#define LXQMLPLUGINS_APPITEMLISTMODEL_H

#include "simpleListModel.h"
#include "appItem.h"

class AppItemListModel : public SimpleListModel<AppItem> {
    Q_OBJECT

public:
    AppItemListModel(QObject *parent = nullptr) : SimpleListModel("app", parent) {}

    Q_INVOKABLE void moveLeft(int index) {
        SimpleListModel::moveLeft(index);
    }

    Q_INVOKABLE void moveRight(int index) {
        SimpleListModel::moveRight(index);
    }
};

#endif
