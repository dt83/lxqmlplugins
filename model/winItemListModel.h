#ifndef LXQMLPLUGINS_WINITEMLISTMODEL_H
#define LXQMLPLUGINS_WINITEMLISTMODEL_H

#include "simpleListModel.h"
#include "winItem.h"

class WinItemListModel : public SimpleListModel<WinItem> {
    Q_OBJECT

public:
    WinItemListModel(QObject *parent = nullptr) : SimpleListModel("win", parent) {}

    bool hasWindow(WId winID) const;

    WinItem *removeWindow(WId winID);

    void add(WinItem *winItem);

    virtual int sortKey(WinItem *winItem) const;

private slots:
    void zChanged(WinItem *winItem);
};

#endif
