#ifndef LXQMLPLUGINS_DESKTOPITEM_H
#define LXQMLPLUGINS_DESKTOPITEM_H

#include <QObject>
#include <QSortFilterProxyModel>

#include <kwindowsystem.h>

#include "winItem.h"
#include "winItemListModel.h"

class DesktopItem : public QObject {
    Q_OBJECT

    Q_PROPERTY(QSortFilterProxyModel *windows READ windows CONSTANT)
    Q_PROPERTY(int index READ index CONSTANT)
    Q_PROPERTY(bool isCurrent READ isCurrent NOTIFY isCurrentChanged)

public:
    DesktopItem(int index, QObject *parent = nullptr);

    QSortFilterProxyModel *windows() const { return m_sortedWinItems; }

    void addWindow(WinItem *winItem);

    WinItem *removeWindow(WId windowID);

    int index() const { return m_index; }

    bool isCurrent() const { return m_isCurrent; }
    void setIsCurrent(bool isCurrent) {
        m_isCurrent = isCurrent;
        emit isCurrentChanged(this);
    }

    Q_INVOKABLE void activate() const;

signals:
    void isCurrentChanged(DesktopItem *item);

private:
    int m_index = 0;
    WinItemListModel *m_winItems;
    QSortFilterProxyModel *m_sortedWinItems;
    bool m_isCurrent = false;

    void winItemDestroyed(WinItem *item);
};

#endif
