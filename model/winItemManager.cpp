#include <KStartupInfo>

#include "winItemManager.h"

WinItemManager::WinItemManager(QObject *parent) : QObject(parent) {
    connect(
        KWindowSystem::self(),
        static_cast<void (KWindowSystem::*)(WId, NET::Properties, NET::Properties2)>(
            &KWindowSystem::windowChanged
        ),
        this, &WinItemManager::windowPropertiesChanged
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::windowAdded,
        this, &WinItemManager::windowOpened
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::windowRemoved,
        this, &WinItemManager::windowClosed
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::activeWindowChanged,
        this, &WinItemManager::activeWindowChanged
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::currentDesktopChanged,
        this, &WinItemManager::currentDesktopChanged
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::stackingOrderChanged,
        this, &WinItemManager::windowStackingOrderChanged
    );
}

void WinItemManager::init() {
    for (WId winID : KWindowSystem::windows()) {
        windowOpened(winID);
    }
}

void WinItemManager::windowPropertiesChanged(WId winID,
        NET::Properties properties, NET::Properties2 properties2) {
    WinItem *winItem = m_winID2item.value(winID);
    if (winItem == nullptr) {
        windowOpened(winID);
        return;
    }
    bool nameChanged = properties & (NET::Property::WMIconName | NET::Property::WMName);
    bool desktopChanged = properties & NET::Property::WMDesktop;
    bool geometryChanged = properties & NET::Property::WMGeometry;
    bool visibilityChanged = properties & (NET::Property::WMState | NET::Property::WMWindowType);
    bool demandsAttentionChanged = properties & NET::Property::WMState;
    bool minimizedChanged = properties & (NET::Property::WMState | NET::Property::XAWMState);
    if (nameChanged) {
        properties = properties | NET::Property::WMIconName | NET::Property::WMName;
    }
    if (visibilityChanged) {
        properties = properties | NET::Property::WMState | NET::Property::WMWindowType;
    }
    if (minimizedChanged) {
        properties = properties | NET::Property::WMState | NET::Property::XAWMState;
    }
    KWindowInfo winInfo(winID, properties, properties2);
    if (nameChanged) {
        winItem->setTitle(winInfo.iconName());
    }
    if (visibilityChanged) {
        if (winInfo.windowType(NET::NormalMask) != NET::WindowType::Normal ||
                winInfo.state() & (NET::State::SkipTaskbar | NET::State::SkipPager)) {
            windowClosed(winID);
        } else if (m_winID2item.value(winID) == nullptr) {
            windowOpened(winID);
        }
    }
    if (demandsAttentionChanged) {
        winItem->setDemandsAttention(winInfo.state() & NET::State::DemandsAttention);
    }
    if (desktopChanged) {
        winItem->setIsOnCurrentDesktop(
            winInfo.desktop() == KWindowSystem::currentDesktop()
        );
        winItem->setDesktop(winInfo.desktop());
    }
    if (geometryChanged) {
        winItem->setGeometry(winInfo.geometry());
    }
    if (minimizedChanged) {
        winItem->setIsMinimized(winInfo.isMinimized());
    }
}

void WinItemManager::windowOpened(WId winID) {
    KWindowInfo winInfo(
        winID,
        NET::Property::WMName | NET::Property::WMIconName |
        NET::Property::WMState | NET::Property::XAWMState |
        NET::Property::WMDesktop | NET::Property::WMWindowType |
        NET::Property::WMGeometry, NET::Property2::WM2WindowClass
    );
    if (winInfo.windowType(NET::NormalMask) != NET::WindowType::Normal) {
        return;
    }
    if (winInfo.state() & (NET::State::SkipTaskbar | NET::State::SkipPager)) {
        return;
    }
    WinItem *winItem = new WinItem(this);
    winItem->setWinID(winID);
    winItem->setTitle(winInfo.iconName());
    winItem->setClassName(winInfo.windowClassClass());
    winItem->setDesktop(winInfo.desktop());
    winItem->setGeometry(winInfo.geometry());
    winItem->setIsOnCurrentDesktop(
        winInfo.desktop() == KWindowSystem::currentDesktop()
    );
    winItem->setDemandsAttention(winInfo.state() & NET::State::DemandsAttention);
    winItem->setIsMinimized(winInfo.isMinimized());
    winItem->setZ(KWindowSystem::stackingOrder().indexOf(winID));
    bool isActive = KWindowSystem::activeWindow() == winID;
    if (isActive) {
        m_activeWinItem = winItem;
    }
    winItem->setIsActive(isActive);
    m_winID2item[winID] = winItem;
    emit winItemAdded(winItem);
}

void WinItemManager::windowStackingOrderChanged() {
    const QList<WId> &stackingOrder = KWindowSystem::stackingOrder();
    for (WinItem *winItem : m_winID2item.values()) {
        winItem->setZ(stackingOrder.indexOf(winItem->winID()));
    }
    emit stackingOrderChanged();
}

void WinItemManager::windowClosed(WId winID) {
    WinItem *winItem = m_winID2item.value(winID);
    if (winItem == nullptr) {
        return;
    }
    if (winItem == m_activeWinItem) {
        m_activeWinItem = nullptr;
    }
    m_winID2item.remove(winID);
    emit winItemRemoved(winItem);
    winItem->deleteLater();
}

void WinItemManager::activeWindowChanged(WId winID) {
    WinItem *winItem = m_winID2item.value(winID);
    if (winItem == nullptr) {
        return;
    }
    if (m_activeWinItem != nullptr) {
        m_activeWinItem->setIsActive(false);
    }
    m_activeWinItem = winItem;
    winItem->setIsActive(true);
}

void WinItemManager::currentDesktopChanged(int desktop) {
    for (WinItem *winItem : m_winID2item) {
        KWindowInfo winInfo(winItem->winID(), NET::Property::WMDesktop);
        winItem->setIsOnCurrentDesktop(winInfo.desktop() == desktop);
    }
}

QList<WinItem *> WinItemManager::winItems() const {
    return m_winID2item.values();
}

QPixmap WinItemManager::getPixmap(WId winID, int width, int height) const {
    return KWindowSystem::icon(winID, width, height);
}

WinItem *WinItemManager::getWinItem(WId winID) const {
    return m_winID2item.value(winID);
}
