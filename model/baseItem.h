#ifndef LXQMLPLUGINS_BASEITEM_H
#define LXQMLPLUGINS_BASEITEM_H

#include <QObject>
#include <qwindowdefs.h>
#include <QMimeData>

class BaseItem : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(QString className READ className NOTIFY classNameChanged)
    Q_PROPERTY(quint64 winID READ winID NOTIFY winIDChanged)
    Q_PROPERTY(QMimeData *mimeData READ mimeData CONSTANT)
    Q_PROPERTY(bool isActive READ isActive NOTIFY isActiveChanged)
    Q_PROPERTY(
        bool isOnCurrentDesktop READ isOnCurrentDesktop
        NOTIFY isOnCurrentDesktopChanged
    )
    Q_PROPERTY(
        bool demandsAttention READ demandsAttention
        NOTIFY demandsAttentionChanged
    )
    Q_PROPERTY(
        bool isMinimized READ isMinimized NOTIFY isMinimizedChanged
    )

public:
    BaseItem(QObject *parent = nullptr) : QObject(parent), m_mimeData(new QMimeData()) {}
    ~BaseItem() {
        delete m_mimeData;
    }

    const QString &title() const { return m_title; }
    void setTitle(const QString &title) {
        m_title = title;
        emit titleChanged(this);
    }

    const QString &className() const { return m_className; }
    void setClassName(const QString &className) {
        m_className = className;
        emit classNameChanged(this);
    }

    virtual WId winID() const = 0;

    virtual bool isActive() const = 0;

    virtual bool isOnCurrentDesktop() const = 0;

    virtual bool demandsAttention() const = 0;

    virtual bool isMinimized() const = 0;

    QMimeData *mimeData() const { return m_mimeData; };

signals:
    void titleChanged(BaseItem *item);
    void classNameChanged(BaseItem *item);
    void isActiveChanged(BaseItem *item);
    void winIDChanged(BaseItem *item);
    void isOnCurrentDesktopChanged(BaseItem *item);
    void demandsAttentionChanged(BaseItem *item);
    void isMinimizedChanged(BaseItem *item);

protected:
    QString m_title, m_className;
    QMimeData *m_mimeData;
};

#endif
