#ifndef LXQMLPLUGINS_WINITEM_H
#define LXQMLPLUGINS_WINITEM_H

#include <QObject>
#include <QRect>

#include "baseItem.h"

class WinItem : public BaseItem {
    Q_OBJECT

    Q_PROPERTY(QRect geometry READ geometry NOTIFY geometryChanged)
    Q_PROPERTY(int desktop READ desktop NOTIFY desktopChanged)
    Q_PROPERTY(int z READ z NOTIFY zChanged)

public:
    WinItem(QObject *parent = nullptr) : BaseItem(parent) {}

    WId winID() const { return m_winID; }
    void setWinID(WId winID);

    bool isActive() const { return m_isActive; }
    void setIsActive(bool isActive) {
        m_isActive = isActive;
        emit isActiveChanged(this);
    }

    bool isOnCurrentDesktop() const { return m_isOnCurrentDesktop; }
    void setIsOnCurrentDesktop(bool isOnCurrentDesktop) {
        m_isOnCurrentDesktop = isOnCurrentDesktop;
        emit isOnCurrentDesktopChanged(this);
    }

    bool demandsAttention() const { return m_demandsAttention; }
    void setDemandsAttention(bool demandsAttention) {
        m_demandsAttention = demandsAttention;
        emit demandsAttentionChanged(this);
    }

    bool isMinimized() const { return m_isMinimized; }
    void setIsMinimized(bool isMinimized) {
        m_isMinimized = isMinimized;
        emit isMinimizedChanged(this);
    }

    const QRect &geometry() const { return m_geometry; }
    void setGeometry(const QRect &geometry) {
        m_geometry = geometry;
        emit geometryChanged(this);
    }

    int desktop() const { return m_desktop; }
    void setDesktop(int desktop) {
        int oldDesktop = m_desktop;
        m_desktop = desktop;
        emit desktopChanged(this, oldDesktop);
    }

    int z() const { return m_z; }
    void setZ(int z) {
        if (m_z == z) {
            return;
        }
        m_z = z;
        emit zChanged(this);
    }

    Q_INVOKABLE void activate();

signals:
    void geometryChanged(WinItem *item);
    void desktopChanged(WinItem *item, int oldDesktop);
    void zChanged(WinItem *item);

private:
    int m_winID = 0;
    bool m_isActive = false;
    bool m_isOnCurrentDesktop = false;
    bool m_demandsAttention = false;
    bool m_isMinimized = false;
    int m_desktop = 1;
    QRect m_geometry;
    int m_z = 0;

private slots:
    void currentDesktopChanged();
};

#endif
