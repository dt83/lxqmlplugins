#include "winItemListModel.h"

bool WinItemListModel::hasWindow(WId winID) const {
    for (WinItem *item : items()) {
        if (item->winID() == winID) {
            return true;
        }
    }
    return false;
}

WinItem *WinItemListModel::removeWindow(WId winID) {
    int row = 0;
    for (WinItem *item : items()) {
        if (item->winID() == winID) {
            disconnect(
                item, &WinItem::zChanged, this, &WinItemListModel::zChanged
            );
            beginRemoveRows(QModelIndex(), row, row);
            m_items.removeOne(item);
            endRemoveRows();
            return item;
        }
        row++;
    }
    return nullptr;
}

int WinItemListModel::sortKey(WinItem *winItem) const {
    return winItem->z();
}

void WinItemListModel::add(WinItem *winItem) {
    SimpleListModel::add(winItem);
    connect(winItem, &WinItem::zChanged, this, &WinItemListModel::zChanged);
}

void WinItemListModel::zChanged(WinItem *winItem) {
    QVector<int> roles;
    roles << sortRole();
    int row = m_items.indexOf(winItem);
    emit dataChanged(index(row, 0), index(row, 0), roles);
}
