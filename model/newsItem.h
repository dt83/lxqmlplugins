#ifndef LXQMLPLUGINS_NEWSITEM_H
#define LXQMLPLUGINS_NEWSITEM_H

#include <QObject>
#include <QUrl>
#include <QDateTime>

#include "newsFeed.h"

class NewsItem : public QObject {
    Q_OBJECT

    Q_PROPERTY(QUrl link READ link CONSTANT)
    Q_PROPERTY(QString headline READ headline NOTIFY headlineChanged)
    Q_PROPERTY(QString description READ description NOTIFY descriptionChanged)
    Q_PROPERTY(QDateTime pubDate READ pubDate NOTIFY pubDateChanged)
    Q_PROPERTY(NewsFeed *feed READ feed CONSTANT)
    Q_PROPERTY(bool seen READ seen NOTIFY seenChanged)

public:
    NewsItem(const QString &headline, const QUrl &link,
            const QString &description, const QDateTime &pubDate)
        : QObject(nullptr), m_headline(headline)
        , m_description(description), m_link(link), m_pubDate(pubDate) {}

    const QString &headline() const { return m_headline; }
    void setHeadline(const QString &headline) {
        m_headline = headline;
        emit headlineChanged(this);
    }

    const QString &description() const { return m_description; }
    void setDescription(const QString &description) {
        m_description = description;
        emit descriptionChanged(this);
    }

    const QUrl &link() const { return m_link; }

    const QDateTime &pubDate() const { return m_pubDate; }
    void setPubDate(const QDateTime &pubDate) {
        m_pubDate = pubDate;
        emit pubDateChanged(this);
    }

    bool seen() const { return m_seen; }
    void setSeen(bool seen) {
        m_seen = seen;
        emit seenChanged(this);
    }

    NewsFeed *feed() const { return m_feed; }

    Q_INVOKABLE void open() const;

signals:
    void headlineChanged(NewsItem *newsItem);
    void descriptionChanged(NewsItem *newsItem);
    void pubDateChanged(NewsItem *newsItem);
    void seenChanged(NewsItem *newsItem);

private:
    NewsFeed *m_feed;
    QString m_headline, m_description;
    QUrl m_link;
    QDateTime m_pubDate;
    bool m_seen;

    friend NewsFeed;
};

#endif
