#ifndef LXQMLPLUGINS_NEWS_ITEM_MANAGER_H
#define LXQMLPLUGINS_NEWS_ITEM_MANAGER_H

#include <QObject>
#include <QUrl>
#include <QHash>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "newsItem.h"
#include "newsFeed.h"

class NewsItemManager : public QObject {
    Q_OBJECT

public:
    NewsItemManager(QObject *parent = nullptr);

    void addFeed(const QUrl &feedUrl);

    void removeFeed(const QUrl &feedUrl);

    NewsFeed *getFeed(const QUrl &feedUrl) const;

    NewsFeed *getFeed(int feedIndex) const;

    NewsItem *getItem(int itemIndex) const;

    int feedCount() const { return m_feeds.size(); }

    int itemCount() const;

    const QList<QUrl> &feedUrls() const { return m_feedUrls; }

    NewsFeed *getNextFeed(NewsFeed *feed) const;

    NewsFeed *getPreviousFeed(NewsFeed *feed) const;

public slots:
    void update();

signals:
    void newsItemAdded(NewsItem *item);
    void ready();

private:
    QList<QUrl> m_feedUrls;
    QList<NewsItem *> m_items;
    QList<NewsFeed *> m_feeds;
    QNetworkAccessManager m_networkAccessManager;

private slots:
    void updateFeed(const QUrl &feedUrl);
    void feedXmlReady(QNetworkReply *reply);
};

#endif
