#include <QDebug>

#include <kwindowsystem.h>

#include "desktopItem.h"

DesktopItem::DesktopItem(int index, QObject *parent)
        : QObject(parent)
        , m_index(index)
        , m_winItems(new WinItemListModel(this))
        , m_sortedWinItems(new QSortFilterProxyModel(this)) {
    m_sortedWinItems->setSourceModel(m_winItems);
    m_sortedWinItems->setSortRole(m_winItems->sortRole());
}

void DesktopItem::addWindow(WinItem *winItem) {
    m_winItems->add(winItem);
}

WinItem *DesktopItem::removeWindow(WId windowID) {
    return m_winItems->removeWindow(windowID);
}

void DesktopItem::activate() const {
    KWindowSystem::setCurrentDesktop(index());
}
