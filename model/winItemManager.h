#ifndef LXQMLPLUGINS_WINITEM_MANAGER_H
#define LXQMLPLUGINS_WINITEM_MANAGER_H

#include <QObject>
#include <QHash>
#include <QPixmap>

#include <kwindowsystem.h>

#include "winItem.h"

class WinItemManager : public QObject {
    Q_OBJECT

public:
    WinItemManager(QObject *parent = nullptr);

    void init();

    QList<WinItem *> winItems() const;

    WinItem *getWinItem(WId winID) const;

    QPixmap getPixmap(WId winID, int width, int height) const;

signals:
    void winItemAdded(WinItem *winItem);
    void winItemRemoved(WinItem *winItem);
    void stackingOrderChanged();

private:
    QHash<WId, WinItem *> m_winID2item;
    WinItem *m_activeWinItem = nullptr;

private slots:
    void windowOpened(WId winID);
    void windowClosed(WId winID);
    void activeWindowChanged(WId winID);
    void currentDesktopChanged(int desktop);
    void windowPropertiesChanged(
        WId winID, NET::Properties properties, NET::Properties2 properties2
    );
    void windowStackingOrderChanged();
};

#endif
