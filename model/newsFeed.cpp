#include <QDesktopServices>

#include "newsItem.h"
#include "newsFeed.h"

NewsFeed::NewsFeed(const QString &title, const QUrl &url,
        const QUrl &link, const QUrl &iconUrl, const QList<NewsItem *> &items,
        QObject *parent)
        : QObject(parent), m_title(title), m_url(url), m_link(link),
        m_iconUrl(iconUrl) {
    setItems(items);
}
    
void NewsFeed::setItems(const QList<NewsItem *> &items) {
    m_items.clear();
    for (NewsItem *item : items) {
        item->m_feed = this;
        item->setParent(this);
        m_items.append(item);
    } 
    emit itemsChanged();
}

void NewsFeed::open() const {
    QDesktopServices::openUrl(m_link);
}

NewsItem *NewsFeed::getItem(const QUrl &link) {
    for (QObject *item : m_items) {
        NewsItem *newsItem = static_cast<NewsItem *>(item);
        if (newsItem->link() == link) {
            return newsItem;
        }
    }
    return nullptr;
}
