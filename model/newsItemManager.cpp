#include <algorithm>

#include <QTime>
#include <QDomDocument>

#include "newsItemManager.h"

NewsItemManager::NewsItemManager(QObject *parent) : QObject(parent) {
    connect(
        &m_networkAccessManager, &QNetworkAccessManager::finished,
        this, &NewsItemManager::feedXmlReady
    );
}

void NewsItemManager::addFeed(const QUrl &feedUrl) {
    m_feedUrls << feedUrl;
    updateFeed(feedUrl);
}

void NewsItemManager::removeFeed(const QUrl &feedUrl) {
    NewsFeed *feed = getFeed(feedUrl);
    if (feed == nullptr) {
        return;
    }
    for (QObject *item : feed->items()) {
        m_items.removeOne(static_cast<NewsItem *>(item));
    }
    m_feeds.removeOne(feed);
    feed->deleteLater();
}

void NewsItemManager::update() {
    for (const QUrl & feedUrl : m_feedUrls) {
        updateFeed(feedUrl);
    }
}

void NewsItemManager::updateFeed(const QUrl &feedUrl) {
    QNetworkRequest request(feedUrl);
    m_networkAccessManager.get(request);
}

int NewsItemManager::itemCount() const {
    int count = 0;
    for (NewsFeed *feed : m_feeds) {
        count += feed->items().size();
    }
    return count;
}

NewsItem *NewsItemManager::getItem(int itemIndex) const {
    return m_items.value(itemIndex);
}

NewsFeed *NewsItemManager::getFeed(const QUrl &feedUrl) const {
    for (NewsFeed *feed : m_feeds) {
        if (feed->url() == feedUrl) {
            return feed;
        }
    }
    return nullptr;
}

NewsFeed *NewsItemManager::getFeed(int feedIndex) const {
    return m_feeds.value(feedIndex);
}

void NewsItemManager::feedXmlReady(QNetworkReply *reply) {
    QUrl feedUrl = reply->request().url();

    NewsFeed *feed = getFeed(feedUrl);

    QDomDocument doc;
    QByteArray content = reply->readAll();
    if (!doc.setContent(content)) {
        reply->deleteLater();
        return;
    }

    QDomElement docElem = doc.documentElement();
    QDomElement channelElem = docElem.elementsByTagName("channel").at(0).toElement();

    QList<NewsItem *> items;

    QDomNodeList itemNodes = docElem.elementsByTagName("item");
    for (int i = 0; i < itemNodes.size(); i++) {
        QDomElement itemElem = itemNodes.at(i).toElement();
        QString headline =
            itemElem.elementsByTagName("title").at(0).toElement().text();
        headline.remove('\n');
        QString link =
            itemElem.elementsByTagName("link").at(0).toElement().text();
        link.remove('\n');
        QString description =
            itemElem.elementsByTagName("description").at(0).toElement().text();
        QDateTime pubDate;
        QString pubDateStr =
            itemElem.elementsByTagName("pubDate").at(0).toElement().text();
        if (pubDateStr.isEmpty()) {
            pubDateStr =
                itemElem.elementsByTagName("dc:date").at(0).toElement().text();
            if (pubDateStr.isEmpty()) {
                pubDate = QDateTime::currentDateTime();
            } else {
                pubDate = QDateTime::fromString(pubDateStr, Qt::ISODate);
            }
        } else {
            pubDate = QDateTime::fromString(pubDateStr, Qt::RFC2822Date);
        }
        NewsItem *newsItem = nullptr;
        if (feed != nullptr) {
            newsItem = feed->getItem(link);
        }
        if (newsItem == nullptr) {
            newsItem = new NewsItem(headline, link, description, pubDate);
            m_items.append(newsItem);
        } else {
            newsItem->setHeadline(headline);
            newsItem->setDescription(description);
            newsItem->setPubDate(pubDate);
        }
        items.append(newsItem);
    }

    if (feed == nullptr) {
        QString title =
            channelElem.elementsByTagName("title").at(0).toElement().text();
        title.remove('\n');
        QString link =
            channelElem.elementsByTagName("link").at(0).toElement().text();
        link.remove('\n');

        /*QUrl iconUrl =
            channelElem
                .elementsByTagName("image").at(0).toElement()
                .elementsByTagName("url").at(0).toElement().text();*/
        QUrl iconUrl = link;
        iconUrl.setPath("/favicon.ico");
        feed = new NewsFeed(title, feedUrl, link, iconUrl, items, this);
        m_feeds.append(feed);
    } else {
        for (QObject *item : feed->items()) {
            NewsItem *newsItem = static_cast<NewsItem *>(item);
            if (!items.contains(newsItem)) {
                m_items.removeOne(newsItem);
                item->deleteLater();
            }
        }
        feed->setItems(items);
    }

    struct {
        bool operator()(NewsItem *item1, NewsItem *item2) {
            return item1->pubDate() > item2->pubDate();
        }
    } cmp;

    std::sort(m_items.begin(), m_items.end(), cmp);

    reply->deleteLater();
    emit ready();
}

NewsFeed *NewsItemManager::getNextFeed(NewsFeed *feed) const {
    int index = m_feeds.indexOf(feed);
    if (index < 0) {
        return nullptr;
    }
    return m_feeds.at((index + 1) % m_feeds.size());
}

NewsFeed *NewsItemManager::getPreviousFeed(NewsFeed *feed) const {
    int index = m_feeds.indexOf(feed);
    if (index < 0) {
        return nullptr;
    }
    return m_feeds.at(std::abs((index - 1) % m_feeds.size()));
}
