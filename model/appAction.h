#ifndef LXQMLPLUGINS_APPACTION_H
#define LXQMLPLUGINS_APPACTION_H

#include <QObject>

#include "appItem.h"

class AppAction : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString actionID READ actionID CONSTANT)
    Q_PROPERTY(QString title READ title CONSTANT)
    Q_PROPERTY(QString icon READ icon CONSTANT)

public:
    AppAction(const QString &actionID, const QString &title,
            const QString &exec, const QString &icon, AppItem *appItem)
        : QObject(appItem)
        , m_appItem(appItem)
        , m_actionID(actionID)
        , m_title(title)
        , m_exec(exec)
        , m_icon(icon) {}

    const QString &actionID() const { return m_actionID; }

    const QString &title() const { return m_title; }

    const QString &exec() const { return m_exec; }

    const QString &icon() const { return m_icon; }

    Q_INVOKABLE void run();

private:
    AppItem *m_appItem = nullptr;
    QString m_actionID, m_title, m_exec, m_icon;

    friend AppItem;
};

#endif

