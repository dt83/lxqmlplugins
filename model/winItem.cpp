#include <kwindowsystem.h>

#include "winItem.h"

void WinItem::setWinID(WId winID) {
    m_winID = winID;
    QByteArray wnckWindowID(reinterpret_cast<char *>(&winID), sizeof(WId));
    mimeData()->setData("application/x-wnck-window-id", wnckWindowID);
    emit winIDChanged(this);
}

void WinItem::activate() {
    if (!isOnCurrentDesktop()) {
        connect(
            KWindowSystem::self(), &KWindowSystem::currentDesktopChanged,
            this, &WinItem::currentDesktopChanged
        );
        KWindowSystem::setCurrentDesktop(desktop());
    } else {
        KWindowSystem::activateWindow(winID());
    }
}

void WinItem::currentDesktopChanged() {
    disconnect(
        KWindowSystem::self(), &KWindowSystem::currentDesktopChanged,
        this, &WinItem::currentDesktopChanged
    );
    KWindowSystem::activateWindow(winID());
}
