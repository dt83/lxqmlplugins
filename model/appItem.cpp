#include <QProcess>
#include <QTimer>

#include <KStartupInfo>

#include "appItem.h"
#include "appAction.h"

AppItem::AppItem(QObject *parent)
        : BaseItem(parent)
        , m_winItems(new WinItemListModel(this)) {
   connect(
       m_winItems, &QAbstractListModel::rowsRemoved,
       this, &AppItem::rowsRemoved
   );
   connect(
       m_winItems, &QAbstractListModel::rowsInserted,
       this, &AppItem::rowsInserted
   );
}

void AppItem::addAction(AppAction *action) {
    m_actions.append(action);
    action->setParent(this);
    action->m_appItem = this;
}

WId AppItem::winID() const {
    WinItem *winItem = m_winItems->get(0);
    if (winItem == nullptr) {
        return 0;
    }
    return winItem->winID();
}

int AppItem::winItemCount() const {
    return m_winItems->rowCount();
}

bool AppItem::isActive() const {
    for (WinItem *item : m_winItems->items()) {
        if (item->isActive()) {
            return true;
        }
    }
    return false;
}

bool AppItem::isOnCurrentDesktop() const {
    for (WinItem *item : m_winItems->items()) {
        if (item->isOnCurrentDesktop()) {
            return true;
        }
    }
    return false;
}

bool AppItem::demandsAttention() const {
    for (WinItem *item : m_winItems->items()) {
        if (item->demandsAttention()) {
            return true;
        }
    }
    return false;
}

bool AppItem::isMinimized() const {
    if (winItemCount() == 0) {
        // An app item without windows is not minimized.
        return false;
    }
    if (isOnCurrentDesktop()) {
        // If there is at least one unminimized window on the current desktop,
        // the app item is not minimized.
        for (WinItem *item : m_winItems->items()) {
            if (item->isOnCurrentDesktop() && !item->isMinimized()) {
                return false;
            }
        }
    } else {
        // If there is at least one unminimized window, the app item is not
        // minimized.
        for (WinItem *item : m_winItems->items()) {
            if (!item->isMinimized()) {
                return false;
            }
        }
    }
    // Otherwise, the app item is minimized.
    return true;
}

void AppItem::updateWinIDMimeData() {
    QMimeData *winItemMimeData = nullptr;
    if (m_winItems->rowCount() == 1) {
        winItemMimeData = m_winItems->get(0)->mimeData();
    } else {
        for (WinItem *winItem : m_winItems->items()) {
            if (winItem->isActive()) {
                winItemMimeData = winItem->mimeData();
                break;
            }
        }
    }
    if (winItemMimeData != nullptr) {
        mimeData()->setData(
            "application/x-wnck-window-id",
            winItemMimeData->data("application/x-wnck-window-id")
        );
    } else {
        mimeData()->removeFormat("application/x-wnck-window-id");
    }
}

void AppItem::rowsRemoved() {
    emit isActiveChanged(this);
    emit isOnCurrentDesktopChanged(this);
    emit winItemCountChanged(this);
    emit demandsAttentionChanged(this);
    emit isMinimizedChanged(this);
    updateWinIDMimeData();
}

void AppItem::rowsInserted() {
    emit isActiveChanged(this);
    emit isOnCurrentDesktopChanged(this);
    emit winItemCountChanged(this);
    emit demandsAttentionChanged(this);
    emit isMinimizedChanged(this);
    updateWinIDMimeData();
}

void AppItem::winItemIsActiveChanged(BaseItem *winItem) {
    Q_UNUSED(winItem)
    emit isActiveChanged(this);
    // HACK for applications not opening a new window, but activating a previous one.
    sendFinish();
    updateWinIDMimeData();
}

void AppItem::winItemIsOnCurrentDesktopChanged(BaseItem *winItem) {
    Q_UNUSED(winItem)
    emit isOnCurrentDesktopChanged(this);
    emit isMinimizedChanged(this);
}

void AppItem::winItemDemandsAttentionChanged(BaseItem *winItem) {
    Q_UNUSED(winItem)
    // HACK for applications not opening a new window, but activating a previous one.
    sendFinish();
    emit demandsAttentionChanged(this);
}

void AppItem::winItemIsMinimizedChanged(BaseItem *winItem) {
    Q_UNUSED(winItem)
    emit isMinimizedChanged(this);
}

void AppItem::addWinItem(WinItem *winItem) {
    m_winItems->add(winItem);
    updateWinIDMimeData();
    connect(
        winItem, &BaseItem::isActiveChanged,
        this, &AppItem::winItemIsActiveChanged
    );
    connect(
        winItem, &BaseItem::isOnCurrentDesktopChanged,
        this, &AppItem::winItemIsOnCurrentDesktopChanged
    );
    connect(
        winItem, &BaseItem::demandsAttentionChanged,
        this, &AppItem::winItemDemandsAttentionChanged
    );
    connect(
        winItem, &BaseItem::isMinimizedChanged,
        this, &AppItem::winItemIsMinimizedChanged
    );
    emit isActiveChanged(this);
    emit isOnCurrentDesktopChanged(this);
    emit demandsAttentionChanged(this);
    emit isMinimizedChanged(this);
    emit winItemCountChanged(this);
}

bool AppItem::hasWindow(WId winID) const {
    return m_winItems->hasWindow(winID);
}

void AppItem::run(QString exec) {
    exec.replace("%u", "", Qt::CaseInsensitive);
    exec.replace("%f", "", Qt::CaseInsensitive);
    KStartupInfoId startupId;
    KStartupInfoData startupData;
    m_startupId = KStartupInfo::createNewStartupId();
    startupId.initId(m_startupId);
    startupData.setApplicationId(desktopEntryPath());
    startupData.setWMClass(appID().toUtf8());
    KStartupInfo::sendStartup(startupId, startupData);
    QProcess::startDetached(exec);
    QTimer::singleShot(30000, this, &AppItem::sendFinish);
}

void AppItem::run() {
    run(this->exec());
}

void AppItem::sendFinish() {
    if (m_startupId.isEmpty()) {
        return;
    }
    KStartupInfoId startupId;
    KStartupInfoData startupData;
    startupId.initId(m_startupId);
    startupData.setApplicationId(desktopEntryPath());
    startupData.setWMClass(appID().toUtf8());
    KStartupInfo::sendFinish(startupId, startupData);
    m_startupId = QByteArray();
}

void AppItem::activate() {
    if (winItemCount() == 0) {
        run();
        return;
    }
    for (int i = 0; i < m_winItems->rowCount(); i++) {
        WinItem *item = m_winItems->get(i);
        if (item->isActive()) {
            m_winItems->get((i + 1) % m_winItems->rowCount())->activate();
            return;
        }
    }
    m_winItems->get(0)->activate();
}
