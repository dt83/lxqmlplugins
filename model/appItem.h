#ifndef LXQMLPLUGINS_APPITEM_H
#define LXQMLPLUGINS_APPITEM_H

#include <QObject>

#include "baseItem.h"
#include "winItem.h"
#include "winItemListModel.h"

class AppAction;

class AppItem : public BaseItem {
    Q_OBJECT

    Q_PROPERTY(WinItemListModel *winItems READ winItems CONSTANT)
    Q_PROPERTY(QList<QObject *> actions READ actions CONSTANT)
    Q_PROPERTY(QString exec READ exec NOTIFY execChanged)
    Q_PROPERTY(QString appID READ appID NOTIFY appIDChanged)
    Q_PROPERTY(QString desktopEntryPath READ desktopEntryPath NOTIFY desktopEntryPathChanged)
    Q_PROPERTY(bool isPinned READ isPinned NOTIFY isPinnedChanged)
    Q_PROPERTY(int winItemCount READ winItemCount NOTIFY winItemCountChanged)
    Q_PROPERTY(bool isStarting READ isStarting NOTIFY isStartingChanged)

public:
    AppItem(QObject *parent = nullptr);

    WinItemListModel *winItems() const { return m_winItems; }
    QList<QObject *> actions() const { return m_actions; }

    void addWinItem(WinItem *winItem);

    WId winID() const;

    int winItemCount() const;

    bool hasWindow(WId winID) const;

    bool isActive() const;

    bool isOnCurrentDesktop() const;

    bool demandsAttention() const;

    bool isMinimized() const;

    const QString &appID() const { return m_appID; }
    void setAppID(const QString &appID) {
        m_appID = appID;
        emit appIDChanged(this);
    }

    const QString &desktopEntryPath() const { return m_desktopEntryPath; }
    void setDesktopEntryPath(const QString &desktopEntryPath) {
        m_desktopEntryPath = desktopEntryPath;
        emit desktopEntryPathChanged(this);
    }

    void addAction(AppAction *action);

    bool isPinned() const { return m_isPinned; }
    void setIsPinned(bool isPinned) {
        m_isPinned = isPinned;
        emit isPinnedChanged(this);
    }

    bool isStarting() const { return m_isStarting; }
    void setIsStarting(bool isStarting) {
        m_isStarting = isStarting;
        emit isStartingChanged(this);
    }

    const QString &exec() const {
        return m_exec;
    }
    void setExec(const QString &exec) {
        m_exec = exec;
        emit execChanged(this);
    }

    Q_INVOKABLE void activate();

    Q_INVOKABLE void run();

signals:
    void appIDChanged(AppItem *item);
    void desktopEntryPathChanged(AppItem *item);
    void isPinnedChanged(AppItem *item);
    void isStartingChanged(AppItem *item);
    void winItemCountChanged(AppItem *item);
    void execChanged(AppItem *item);

private:
    WinItemListModel *m_winItems;
    QList<QObject *> m_actions;
    QString m_appID, m_desktopEntryPath, m_exec;
    bool m_isPinned = false;
    bool m_isStarting = false;
    QByteArray m_startupId;

    void run(QString exec);

    friend AppAction;

private slots:
    void rowsRemoved();
    void rowsInserted();
    void winItemIsActiveChanged(BaseItem *item);
    void winItemIsOnCurrentDesktopChanged(BaseItem *item);
    void winItemDemandsAttentionChanged(BaseItem *item);
    void winItemIsMinimizedChanged(BaseItem *item);
    void updateWinIDMimeData();
    void sendFinish();
};

#endif
