#include <QDesktopServices>

#include "newsItem.h"

void NewsItem::open() const {
    QDesktopServices::openUrl(m_link);
}
