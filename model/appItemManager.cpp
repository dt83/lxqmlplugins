#include <QIcon>

#include <kservice.h>

#include "appAction.h"
#include "appItemManager.h"

AppItemManager::AppItemManager(WinItemManager *winItemManager, QObject *parent)
        : QObject(parent), m_winItemManager(winItemManager), m_startupInfo(KStartupInfo::CleanOnCantDetect, this) {
    connect(
        m_winItemManager, &WinItemManager::winItemAdded,
        this, &AppItemManager::winItemAdded
    );
    QStringList themeSearchPaths = QIcon::themeSearchPaths();
    themeSearchPaths << "/usr/share/pixmaps";
    QIcon::setThemeSearchPaths(themeSearchPaths);
    connect(
        &m_startupInfo, &KStartupInfo::gotNewStartup,
        this, &AppItemManager::gotNewStartup
    );
    connect(
        &m_startupInfo, &KStartupInfo::gotRemoveStartup,
        this, &AppItemManager::gotRemoveStartup
    );
    connect(
        &m_startupInfo, &KStartupInfo::gotRemoveStartup,
        this, &AppItemManager::gotRemoveStartup
    );
}

void AppItemManager::gotNewStartup(const KStartupInfoId &id, const KStartupInfoData &data) {
    Q_UNUSED(id)
    const QString &desktopEntryPath = data.applicationId();
    if (desktopEntryPath.isEmpty() || data.WMClass().isEmpty()) {
        return;
    }
    for (AppItem *appItem : m_appItems) {
        if (desktopEntryPath == appItem->desktopEntryPath()) {
            appItem->setClassName(data.WMClass());
            appItem->setIsStarting(true);
            return;
        }
    }
    KService::Ptr service = KService::serviceByDesktopPath(desktopEntryPath);
    if (service == nullptr) {
        return;
    }
    AppItem *appItem = new AppItem(this);
    appItem->setClassName(data.WMClass());
    appItem->setIsStarting(true);
    initAppItemFromService(appItem, service);
    addAppItem(appItem);
}

void AppItemManager::gotStartupChange(const KStartupInfoId &id, const KStartupInfoData &data) {
    Q_UNUSED(id)
    const QString &desktopEntryPath = data.applicationId();
    for (AppItem *appItem : m_appItems) {
        if (desktopEntryPath == appItem->desktopEntryPath()) {
            appItem->setClassName(data.WMClass());
            appItem->setIsStarting(true);
            return;
        }
    }
}

void AppItemManager::gotRemoveStartup(const KStartupInfoId &id, const KStartupInfoData &data) {
    Q_UNUSED(id)
    const QString &desktopEntryPath = data.applicationId();
    for (AppItem *appItem : m_appItems) {
        if (desktopEntryPath == appItem->desktopEntryPath()) {
            appItem->setIsStarting(false);
            return;
        }
    }
}

void AppItemManager::init() {
    m_winItemManager->init();
}

void AppItemManager::winItemAdded(WinItem *winItem) {
    KStartupInfoId startupId;
    if (m_startupInfo.checkStartup(winItem->winID(), startupId)) {
        KStartupInfo::sendFinish(startupId);
    }
    for (AppItem *appItem : m_appItems) {
        if (appItem->className().toLower() == winItem->className().toLower()) {
            appItem->addWinItem(winItem);
            return;
        }
    }
    AppItem *appItem = new AppItem(this);
    appItem->setClassName(winItem->className());
    appItem->addWinItem(winItem);
    appItem->setTitle(winItem->title());
    KService::Ptr service = KService::serviceByStorageId(winItem->className().toLower());
    if (service == nullptr) {
        service = KService::serviceByDesktopName(winItem->className().toLower());
    }
    if (service == nullptr) {
        service = KService::serviceByMenuId(winItem->className().toLower());
    }
    initAppItemFromService(appItem, service);
    addAppItem(appItem);
}

void AppItemManager::removeAppItem(const QString &appID) {
    AppItem *appItem = getAppItem(appID);
    if (appItem == nullptr) {
        return;
    }
    removeAppItem(appItem);
}

void AppItemManager::removeAppItem(AppItem *appItem) {
    m_appItems.removeOne(appItem);
    emit appItemRemoved(appItem);
    appItem->deleteLater();
}

void AppItemManager::winItemCountChanged(AppItem *appItem) {
    if (appItem->winItemCount() == 0 && !appItem->isPinned()) {
        // This will also remove it from m_appItems.
        removeAppItem(appItem);
    }
}

void AppItemManager::addAppItem(AppItem *appItem) {
    connect(
        appItem, &AppItem::winItemCountChanged,
        this, &AppItemManager::winItemCountChanged
    );
    m_appItems << appItem;
    emit appItemAdded(appItem);
}

void AppItemManager::addAppItem(const QString &appID, const QString &className) {
    AppItem *appItem = new AppItem(this);
    appItem->setClassName(className);
    appItem->setAppID(appID);
    appItem->setIsPinned(true);
    initAppItemFromService(appItem, KService::serviceByDesktopName(appID));
    addAppItem(appItem);
}

void AppItemManager::initAppItemFromService(AppItem *appItem, KService::Ptr service) {
    if (!service) {
        return;
    }
    appItem->setTitle(service->name());
    appItem->setAppID(service->desktopEntryName());
    appItem->setExec(service->exec());
    appItem->setDesktopEntryPath(service->entryPath());
    QList<QUrl> urls;
    urls.append(QUrl::fromLocalFile(service->entryPath()));
    appItem->mimeData()->setUrls(urls);
    for (const KServiceAction &serviceAction : service->actions()) {
        if (serviceAction.exec().contains("%f", Qt::CaseInsensitive) ||
                serviceAction.exec().contains("%u", Qt::CaseInsensitive)) {
            // Skip file actions.
            continue;
        }
        AppAction *action = new AppAction(
            serviceAction.name(), serviceAction.text(), serviceAction.exec(),
            serviceAction.icon(), appItem
        );
        appItem->addAction(action);
    }
}

AppItem *AppItemManager::getAppItem(const QString &appID) const {
    for (AppItem *appItem : m_appItems) {
        if (appItem->appID() == appID) {
            return appItem;
        }
    }
    return nullptr;
}

QPixmap AppItemManager::getPixmap(const QString &appID, int width, int height) const {
    QPixmap pixmap;

    KService::Ptr service = KService::serviceByDesktopName(appID);
    if (service) {
        const QString &icon = service->icon();
        if (icon.startsWith("/")) {
            pixmap = QIcon(icon).pixmap(width, height);
        } else {
            pixmap = QIcon::fromTheme(service->icon()).pixmap(width, height);
            // HACK for apps which do not install their icons properly.
            if (pixmap.isNull()) {
                for (const QString &iconPath : QIcon::themeSearchPaths()) {
                    // Some .desktop files still have incorrect Icon entries
                    // (including extension).
                    if (service->icon().endsWith(".png") || service->icon().endsWith(".xpm")) {
                        pixmap = QIcon(iconPath + "/" + service->icon()).pixmap(width, height);
                    } else {
                        pixmap = QIcon(iconPath + "/" + service->icon() + ".png").pixmap(width, height);
                        if (pixmap.isNull()) {
                            pixmap = QIcon(iconPath + "/" + service->icon() + ".xpm").pixmap(width, height);
                        }
                    }
                    if (!pixmap.isNull()) {
                        break;
                    }
                }
            }
        }
    }
    // TODO: Default icon.
    return pixmap;
}
