#ifndef LXQMLPLUGINS_APPITEM_MANAGER_H
#define LXQMLPLUGINS_APPITEM_MANAGER_H

#include <QObject>
#include <QPixmap>

#include <kservice.h>
#include <KStartupInfo>
#include <KSelectionOwner>

#include "appItem.h"
#include "winItemManager.h"

class AppItemManager : public QObject {
    Q_OBJECT

public:
    AppItemManager(WinItemManager *winItemManager, QObject *parent = nullptr);

    void init();

    QList<AppItem *> appItems() const;

    void addAppItem(const QString &appID, const QString &className);

    AppItem *getAppItem(const QString &appID) const;

    void removeAppItem(const QString &appID);

    QPixmap getPixmap(const QString &appID, int width, int height) const;

signals:
    void appItemAdded(AppItem *appItem);
    void appItemRemoved(AppItem *appItem);

private:
    QList<AppItem *> m_appItems;
    WinItemManager *m_winItemManager;
    KStartupInfo m_startupInfo;
    KSelectionOwner *m_selection;
    
    void addAppItem(AppItem *appItem);
    void removeAppItem(AppItem *appItem);
    void initAppItemFromService(AppItem *appItem, KService::Ptr service);

private slots:
    void winItemAdded(WinItem *winItem);
    void winItemCountChanged(AppItem *appItem);
    void gotNewStartup(const KStartupInfoId &id, const KStartupInfoData &data);
    void gotStartupChange(const KStartupInfoId &id, const KStartupInfoData &data);
    void gotRemoveStartup(const KStartupInfoId &id, const KStartupInfoData &data);
};

#endif
