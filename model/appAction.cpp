#include <QProcess>

#include "appAction.h"

void AppAction::run() {
    if (m_appItem != nullptr) {
        m_appItem->run(exec());
    }
}
