#include "desktopItemManager.h"

DesktopItemManager::DesktopItemManager(WinItemManager *winItemManager, QObject *parent)
        : QObject(parent)
        , m_winItemManager(winItemManager)
        , m_currentDesktop(KWindowSystem::currentDesktop()) {
    connect(
        m_winItemManager, &WinItemManager::winItemAdded,
        this, &DesktopItemManager::winItemAdded
    );
    connect(
        m_winItemManager, &WinItemManager::winItemRemoved,
        this, &DesktopItemManager::winItemRemoved
    );
    connect(
        m_winItemManager, &WinItemManager::stackingOrderChanged,
        this, &DesktopItemManager::stackingOrderChanged
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::currentDesktopChanged,
        this, &DesktopItemManager::currentDesktopChanged
    );
    connect(
        KWindowSystem::self(), &KWindowSystem::numberOfDesktopsChanged,
        this, &DesktopItemManager::numberOfDesktopsChanged
    );
}

void DesktopItemManager::init() {
    for (int i = 1; i <= KWindowSystem::numberOfDesktops(); i++) {
        DesktopItem *desktopItem = new DesktopItem(i, this);
        desktopItem->setIsCurrent(i == m_currentDesktop);
        m_desktopItems.append(desktopItem);
    }
    m_winItemManager->init();
    for (WinItem *winItem : m_winItemManager->winItems()) {
        winItemAdded(winItem);
    }
}

void DesktopItemManager::numberOfDesktopsChanged(int numberOfDesktops) {
    if (numberOfDesktops > m_desktopItems.size()) {
        for (int i = m_desktopItems.size() + 1; i <= numberOfDesktops; i++) {
            DesktopItem *desktopItem = new DesktopItem(i, this);
            desktopItem->setIsCurrent(i == m_currentDesktop);
            m_desktopItems.append(desktopItem);
            emit desktopItemAdded(desktopItem);
        }
    } else if (numberOfDesktops < m_desktopItems.size()) {
        int oldNumberOfDesktops = m_desktopItems.size();
        for (int i = numberOfDesktops; i < oldNumberOfDesktops; i++) {
            m_desktopItems.takeAt(i)->deleteLater();
        }
    }
}

void DesktopItemManager::stackingOrderChanged() {
    for (DesktopItem *desktopItem : m_desktopItems) {
        desktopItem->windows()->sort(0, Qt::AscendingOrder);
    }
}

void DesktopItemManager::winItemRemoved(WinItem *winItem) {
    disconnect(
        winItem, &WinItem::desktopChanged,
        this, &DesktopItemManager::winItemDesktopChanged
    );
}

void DesktopItemManager::winItemAdded(WinItem *winItem) {
    if (winItem->desktop() == -1) {
        for (DesktopItem *desktopItem : m_desktopItems) {
            desktopItem->addWindow(winItem);
        }
    } else {
        DesktopItem *newDesktopItem = m_desktopItems.value(winItem->desktop() - 1);
        if (newDesktopItem != nullptr) {
            newDesktopItem->addWindow(winItem);
        }
    }
    connect(
        winItem, &WinItem::desktopChanged,
        this, &DesktopItemManager::winItemDesktopChanged
    );
}

void DesktopItemManager::winItemDesktopChanged(WinItem *winItem, int oldDesktop) {
    if (oldDesktop == -1) {
        for (DesktopItem *desktopItem : m_desktopItems) {
            desktopItem->removeWindow(winItem->winID());
        }
    } else {
        DesktopItem *oldDesktopItem = m_desktopItems.value(oldDesktop - 1);
        if (oldDesktopItem != nullptr) {
            oldDesktopItem->removeWindow(winItem->winID());
        }
    }
    if (winItem->desktop() == -1) {
        for (DesktopItem *desktopItem : m_desktopItems) {
            desktopItem->addWindow(winItem);
        }
    } else {
        DesktopItem *newDesktopItem = m_desktopItems.value(winItem->desktop() - 1);
        if (newDesktopItem != nullptr) {
            newDesktopItem->addWindow(winItem);
        }
    }
}

void DesktopItemManager::currentDesktopChanged(int desktop) {
    DesktopItem *oldDesktopItem = m_desktopItems.value(m_currentDesktop - 1);
    if (oldDesktopItem != nullptr) {
        oldDesktopItem->setIsCurrent(false);
    }
    m_currentDesktop = desktop;
    DesktopItem *newDesktopItem = m_desktopItems.value(desktop - 1);
    if (newDesktopItem != nullptr) {
        newDesktopItem->setIsCurrent(true);
    }
}

void DesktopItemManager::swapDesktops(int desktop1, int desktop2) {
    DesktopItem *desktopItem1 = m_desktopItems.value(desktop1 - 1);
    if (desktopItem1 == nullptr) {
        return;
    }
    DesktopItem *desktopItem2 = m_desktopItems.value(desktop2 - 1);
    if (desktopItem2 == nullptr) {
        return;
    }
    for (WId winID : KWindowSystem::stackingOrder()) {
        WinItem *winItem = m_winItemManager->getWinItem(winID);
        if (winItem != nullptr) {
            if (winItem->desktop() == desktop1) {
                KWindowSystem::setOnDesktop(winID, desktop2);
            } else if (winItem->desktop() == desktop2) {
                KWindowSystem::setOnDesktop(winID, desktop1);
            }
        }
    }
    if (KWindowSystem::currentDesktop() == desktop1) {
        KWindowSystem::setCurrentDesktop(desktop2);
    }
}
