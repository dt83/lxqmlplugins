#ifndef LXQMLPLUGINS_DESKTOPITEMLISTMODEL_H
#define LXQMLPLUGINS_DESKTOPITEMLISTMODEL_H

#include "simpleListModel.h"
#include "desktopItem.h"

class DesktopItemListModel : public SimpleListModel<DesktopItem> {
    Q_OBJECT

public:
    DesktopItemListModel(QObject *parent = nullptr) : SimpleListModel("desktop", parent) {}
};

#endif
