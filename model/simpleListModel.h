#ifndef LXQMLPLUGINS_SIMPLE_LIST_MODEL_H
#define LXQMLPLUGINS_SIMPLE_LIST_MODEL_H

#include <QQmlEngine>
#include <QAbstractListModel>

template<typename T>
class SimpleListModel : public QAbstractListModel {

public:
    SimpleListModel(const QByteArray &itemRoleName, QObject *parent = nullptr)
        : QAbstractListModel(parent), m_itemRoleName(itemRoleName) {}

    virtual int sortKey(T *item) const {
        Q_UNUSED(item)
        return 0;
    }

    int sortRole() const {
        return Qt::UserRole + 1;
    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const {
        int row = index.row();
        if (row < 0 || row >= m_items.size()) {
            return QVariant();
        }
        if (role == Qt::UserRole) {
            return QVariant::fromValue(m_items.at(row));
        }
        if (role == sortRole()) {
            return sortKey(m_items.at(row));
        }
        return QVariant();
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const {
        Q_UNUSED(parent)
        return m_items.size();
    }

    QHash<int, QByteArray> roleNames() const {
        QHash<int, QByteArray> roles;
        roles[Qt::UserRole] = m_itemRoleName;
        return roles;
    }

    void add(T *item) {
        QQmlEngine::setObjectOwnership(item, QQmlEngine::CppOwnership);
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_items.append(item);
        endInsertRows();
        connect(item, &QObject::destroyed, this, &SimpleListModel::itemDestroyed);
    }

    void moveLeft(int index) {
        if (index <= 0 || index >= m_items.size()) {
            return;
        }
        beginMoveRows(QModelIndex(), index, index, QModelIndex(), index - 1);
        m_items.insert(index - 1, m_items.takeAt(index));
        endMoveRows();
    }

    void moveRight(int index) {
        if (index >= m_items.size() - 1 || index < 0) {
            return;
        }
        beginMoveRows(QModelIndex(), index, index, QModelIndex(), index + 2);
        m_items.insert(index + 1, m_items.takeAt(index));
        endMoveRows();
    }

    const QList<T *> &items() const { return m_items; }

    T *get(int index) const {
        return m_items.value(index);
    }

    void clear() {
        beginRemoveRows(QModelIndex(), 0, m_items.size() - 1);
        m_items.clear();
        endRemoveRows();
    }

protected:
    QList<T *> m_items;

private:
    QByteArray m_itemRoleName;

    void itemDestroyed(QObject *obj) {
        T *item = static_cast<T *>(obj);
        int index = m_items.indexOf(item);
        beginRemoveRows(QModelIndex(), index, index);
        m_items.removeOne(item);
        endRemoveRows();
    }

};

#endif
