#ifndef LXQMLPLUGINS_NEWS_FEED_H
#define LXQMLPLUGINS_NEWS_FEED_H

#include <QObject>
#include <QUrl>

class NewsItem;

class NewsFeed : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString title READ title CONSTANT)
    Q_PROPERTY(QUrl url READ url CONSTANT)
    Q_PROPERTY(QUrl link READ link CONSTANT)
    Q_PROPERTY(QUrl iconUrl READ iconUrl CONSTANT)
    Q_PROPERTY(QList<QObject *> items READ items CONSTANT)

public:
    NewsFeed(const QString &title, const QUrl &url, const QUrl &link,
            const QUrl &iconUrl, const QList<NewsItem *> &items,
            QObject *parent = nullptr);

    const QString &title() const { return m_title; }

    const QUrl &url() const { return m_url; }

    const QUrl &link() const { return m_link; }

    const QUrl &iconUrl() const { return m_iconUrl; }

    void clear();

    const QList<QObject *> &items() const { return m_items; }

    void setItems(const QList<NewsItem *> &items);

    Q_INVOKABLE void open() const;

    NewsItem *getItem(const QUrl &link);

signals:
    void itemsChanged();

private:
    QString m_title;
    QUrl m_url, m_link, m_iconUrl;
    QList<QObject *> m_items;
};

#endif
