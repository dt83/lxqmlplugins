#ifndef LXQMLPLUGINS_DESKTOP_ITEM_MANAGER_H
#define LXQMLPLUGINS_DESKTOP_ITEM_MANAGER_H

#include <QObject>

#include "desktopItem.h"
#include "winItem.h"
#include "winItemManager.h"

class DesktopItemManager : public QObject {
    Q_OBJECT

public:
    DesktopItemManager(WinItemManager *winItemManager, QObject *parent = nullptr);

    const QList<DesktopItem *> &desktopItems() const { return m_desktopItems; }

    void init();

    void swapDesktops(int desktop1, int desktop2);

signals:
    void desktopItemAdded(DesktopItem *item);

private:
    QList<DesktopItem *> m_desktopItems;
    WinItemManager *m_winItemManager;
    int m_currentDesktop;

    void winItemAdded(WinItem *winItem);
    void winItemRemoved(WinItem *winItem);
    void winItemDesktopChanged(WinItem *winItem, int oldDesktop);
    void currentDesktopChanged(int desktop);
    void stackingOrderChanged();
    void numberOfDesktopsChanged(int numberOfDesktops);
};

#endif
